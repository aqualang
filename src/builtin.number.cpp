
#include "private.h"
#include "builtin.h"

namespace Aqua
{
    using Value = Interpreter::Value;

    namespace Builtins
    {
        //BUILTIN_PROTO(rtbuiltin_func_string_length, ip, self, args)

        //void fn_ord(Interpreter&, std::vector<Interpreter::Value> &stack)
        BUILTIN_PROTO(rtbuiltin_func_number_ord, ip, self, args)
        {
            (void)ip;
            (void)self;
            (void)args;
            return Interpreter::Value();
        }

        //void fn_chr(Interpreter&, std::vector<Interpreter::Value> &stack)
        BUILTIN_PROTO(rtbuiltin_func_number_chr, ip, self, args)
        {
            (void)ip;
            (void)args;
            auto selfnum = self.number();
            auto num = static_cast<int>(std::round(selfnum));
            auto ch = char(num);
            return Interpreter::Value(std::string(&ch, 1));
        }
    }
}
