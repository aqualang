
#include "private.h"
#include "builtin.h"

//! todo: type checking - maybe add more helper methods in Interpreter

namespace Aqua
{
    namespace Builtins
    {
        static void make_mode(const std::string& str, std::ios_base::openmode& dest)
        {
            // laaaaazy
            if(str == "r")
            {
                dest = std::ios::in;
            }
        }

        OBJMEMBER_PROTO(File, constructor, ip, stack)
        {
            std::cerr << "in File::constructor" << std::endl;
            std::string path;
            std::string mode;
            std::ios_base::openmode realmode;
            realmode = std::ios::in;
            Table<Interpreter::Value> self;
            if(stack.empty())
            {
                std::cerr << "File() expects 2 arguments" << std::endl;
                return;
            }
            path = stack[1].string();
            mode = stack[2].string();
            make_mode(mode, realmode);
            auto fh = new std::fstream(path, realmode);
            if(fh->good())
            {
                fprintf(stderr, "File.constructor: fh=%p\n", fh);
                // create "self" object - similar concept as in Javascript
                auto tb = ip.makeTable();
                tb->set(ID_HANDLE, ip.makeUserPointer(fh));
                for(auto pair: FileMembers)
                {
                    tb->set(pair.first, ip.makeFunction(pair.second));
                }
                stack.emplace_back(tb);
                return;
            }
            else
            {
                std::cerr << "fopen('" << path << "', '" << mode << "') failed" << std::endl;
                stack.clear();
                return;
            }

        }

        OBJMEMBER_PROTO(File, proto_typeof, ip, stack)
        {
            stack.emplace_back(ip.makeString("file"));
        }

        OBJMEMBER_PROTO(File, close, ip, stack)
        {
            (void)ip;
            auto self = stack[0].table();
            auto fh = (std::fstream*)self->get(ID_HANDLE).userptr();
            delete fh;
        }

        OBJMEMBER_PROTO(File, flush, ip, stack)
        {
            (void)ip;
            auto self = stack[0].table();
            auto fh = (std::ostream*)self->get(ID_HANDLE).userptr();
            (*fh) << std::flush;
        }


        OBJMEMBER_PROTO(File, read, ip, stack)
        {
            char* buffer;
            size_t bufsize;
            size_t realsize;
            auto self = stack[0].table();
            auto fh = (std::istream*)self->get(ID_HANDLE).userptr();
            bufsize = size_t(stack[1].number());
            buffer = new char[bufsize + 1];
            fh->read(buffer, bufsize);
            realsize = fh->gcount();
            stack.emplace_back(ip.makeString(std::string(buffer, realsize)));
            delete[] buffer;
            return;
        }

        OBJMEMBER_PROTO(File, readAll, ip, stack)
        {
            auto self = stack[0].table();
            auto fh = (std::istream*)self->get(ID_HANDLE).userptr();
            std::string filestr((std::istreambuf_iterator<char>(*fh)), (std::istreambuf_iterator<char>()));
            stack.emplace_back(ip.makeString(filestr));
            return;
        }

        OBJMEMBER_PROTO(File, readChar, ip, stack)
        {
            (void)ip;
            (void)stack;
        }

        OBJMEMBER_PROTO(File, readByte, ip, stack)
        {
            (void)ip;
            (void)stack;
        }

        OBJMEMBER_PROTO(File, readLines, ip, stack)
        {
            (void)ip;
            (void)stack;
        }

        OBJMEMBER_PROTO(File, readLine, ip, stack)
        {
            (void)ip;
            (void)stack;
        }

        OBJMEMBER_PROTO(File, print, ip, stack)
        {
            (void)ip;
            auto self = stack[0].table();
            auto fh = (std::ostream*)self->get(ID_HANDLE).userptr();
            for(size_t i=1; i<stack.size(); i++)
            {
                (*fh) << stack[i];
            }
            stack.clear();
        }

        OBJMEMBER_PROTO(File, println, ip, stack)
        {
            (void)ip;
            auto self = stack[0].table();
            auto fh = (std::ostream*)self->get(ID_HANDLE).userptr();
            for(size_t i=1; i<stack.size(); i++)
            {
                (*fh) << stack[i];
            }
            (*fh) << std::endl;
            stack.clear();
        }

        OBJMEMBER_PROTO(File, write, ip, stack)
        {
            size_t write_this_much;
            size_t writtensize;
            size_t before;
            Interpreter::Value raw_thismuch;
            Interpreter::Value chunk;
            if((ip.needString(stack[1], chunk)) && ip.needNumber(stack[2], raw_thismuch))
            {
                auto self = stack[0].table();
                auto fh = (std::ostream*)self->get(ID_HANDLE).userptr();
                before = fh->tellp();
                write_this_much = size_t(raw_thismuch.number());
                if(fh->write(chunk.string().data(), write_this_much))
                {
                    writtensize = (size_t(fh->tellp()) - before);
                    stack.emplace_back(ip.makeNumber(writtensize));
                    return;
                }
            }
            stack.clear();
        }

        OBJMEMBER_PROTO(File, writeByte, ip, stack)
        {
            Interpreter::Value thing;
            auto self = stack[0].table();
            auto fh = (std::ostream*)self->get(ID_HANDLE).userptr();
            if(ip.needNumber(stack[1], thing))
            {
                (*fh) << char(thing.number());
            }
            stack.clear();
        }
    }
}
