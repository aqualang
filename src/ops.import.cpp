
#include "private.h"

namespace Aqua
{
    using namespace Internal;

    template<typename Node>
    static Interpreter::Value handle_import(Interpreter& ip, Node& node, Interpreter::Value& expr)
    {
        bool failed;
        std::stringstream errstrm;
        auto modpath = ip.getModulePath();
        failed = true;
        for(auto path: modpath)
        {
            std::stringstream filepath;
            std::fstream fh;
            filepath << path << "/" << expr.string() << ".aqm";
            fh.open(filepath.str(), std::ios::in);
            if(fh.good())
            {
                std::string filedata(
                    (std::istreambuf_iterator<char>(fh)),
                    std::istreambuf_iterator<char>()
                );
                failed = false;
                return ip.eval(filedata);
            }
        }
        if(failed)
        {
            errstrm << "could not import '" << expr.string() << "' (path: [";
            for(auto iter=modpath.begin(); iter!=modpath.end(); iter++)
            {
                errstrm << "\"" << *iter << "\"";
                if((iter + 1) != modpath.end())
                {
                    errstrm << ", ";
                }
            }
            errstrm << "]";
            ip.error(errstrm.str(), node);
        }
        return Interpreter::Value();
    }

    template<typename Node>
    static Interpreter::Value import_main(Interpreter& ip, Node& node, Interpreter::Value& expr)
    {
        std::stringstream errstrm;
        switch(expr.type())
        {
            case Interpreter::Value::Type::String:
                return handle_import(ip, node, expr);
            /*case Interpreter::Value::Type::Array:
                for(auto& val: expr.array()->get())
                {
                    return import_main(ip);
                }
            */
            default:
                break;
        }
        errstrm << "'import' expects a string, got " << expr.typeName() << "instead";
        ip.error(errstrm.str(), node);
        return Interpreter::Value();
    }

    Interpreter::Value Interpreter::operator()(DoImport<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value expr;
        expr = node.children[0]->accept(*this);
        std::cerr << "import: args = " << expr << std::endl;
        return import_main(*this, node, expr);
        return Interpreter::Value();
    }
}

