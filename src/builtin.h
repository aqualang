
#include <map>

#define BUILTIN_PROTO(funcname, ipname, selfname, argsname) \
    Interpreter::Value funcname( \
        Interpreter& ipname, \
        Interpreter::Value& selfname, \
        std::vector<Interpreter::Value>& argsname \
    )

#define BUILTIN_DECLARE(funcname) \
    BUILTIN_PROTO(funcname, _ip, _self, _args)

#define BUILTIN_EXISTS(shortname, funcname) \
    ( \
        shortname##Members.find(funcname) != shortname##Members.end() \
    )

#define BUILTIN_GET(shortname, funcname) \
    shortname##Members[funcname]


#define OBJMEMBER_NAME(base, name) \
    rt_objmember_func_##base##_##name

#define OBJMEMBER_PROTO(base, name, ipname, stackname) \
    void OBJMEMBER_NAME(base, name)(Interpreter& ipname, std::vector<Interpreter::Value>& stackname)

#define OBJMEMBER_DECLARE(base, name) \
    OBJMEMBER_PROTO(base, name, a, b)

namespace Aqua
{
    using BuiltinFunction = std::function<Interpreter::Value(
        Interpreter&,
        Interpreter::Value&,
        std::vector<Interpreter::Value>&
    )>;

    namespace Builtins
    {
    //namespace FileClass
    //{
        OBJMEMBER_DECLARE(File, constructor);
        OBJMEMBER_DECLARE(File, proto_typeof);
        OBJMEMBER_DECLARE(File, close);
        OBJMEMBER_DECLARE(File, flush);
        OBJMEMBER_DECLARE(File, read);
        OBJMEMBER_DECLARE(File, readAll);
        OBJMEMBER_DECLARE(File, readChar);
        OBJMEMBER_DECLARE(File, readByte);
        OBJMEMBER_DECLARE(File, readLines);
        OBJMEMBER_DECLARE(File, readLine);
        OBJMEMBER_DECLARE(File, print);
        OBJMEMBER_DECLARE(File, println);
        OBJMEMBER_DECLARE(File, write);
        OBJMEMBER_DECLARE(File, writeByte);

        static const std::map<std::string, Interpreter::FuncProto> FileMembers =
        {
            {"__typeof",  OBJMEMBER_NAME(File, proto_typeof)},
            {"close",     OBJMEMBER_NAME(File, close)},
            {"flush",     OBJMEMBER_NAME(File, flush)},
            {"read",      OBJMEMBER_NAME(File, read)},
            {"readAll",   OBJMEMBER_NAME(File, readAll)},
            {"readLines", OBJMEMBER_NAME(File, readLines)},
            {"readLine",  OBJMEMBER_NAME(File, readLine)},
            {"readChar",  OBJMEMBER_NAME(File, readChar)},
            {"readByte",  OBJMEMBER_NAME(File, readByte)},
            {"print",     OBJMEMBER_NAME(File, print)},
            {"println",   OBJMEMBER_NAME(File, println)},
            {"write",     OBJMEMBER_NAME(File, write)},
            {"writeByte", OBJMEMBER_NAME(File, writeByte)},
        };

        //! function protos for string builtins
        BUILTIN_DECLARE(rtbuiltin_func_string_length);
        BUILTIN_DECLARE(rtbuiltin_func_string_substr);

        //! function protos for array builtins
        BUILTIN_DECLARE(rtbuiltin_func_array_length);

        //! function protos for numbers
        BUILTIN_DECLARE(rtbuiltin_func_number_chr);
        BUILTIN_DECLARE(rtbuiltin_func_number_ord);

        //! function protos for function type builtins (will also apply to tree type)

        /** -------------------------------------- **/

        //! builtin functions for strings
        static std::map<std::string, BuiltinFunction> StringMembers =
        {
            {"length",     rtbuiltin_func_string_length},
            {"size",       rtbuiltin_func_string_length},
            {"substr",     rtbuiltin_func_string_substr},
        #if 0
            {"startsWith", },
            {"endsWith",   },
            {"format",     },
            {"fmt",        },
            {"toLower",    },
            {"toUpper",    },
            {"capitalize", },
            {"at",         },
        #endif
        };


        //! builtin functions for arrays
        static std::map<std::string, BuiltinFunction> ArrayMembers =
        {
            {"length", rtbuiltin_func_array_length},
            {"size",   rtbuiltin_func_array_length},
        };

        //! builtin functions for numbers
        static std::map<std::string, BuiltinFunction> NumberMembers =
        {
            {"chr",    rtbuiltin_func_number_chr},
            {"toChar", rtbuiltin_func_number_chr},
            {"ord",    rtbuiltin_func_number_ord},
        };

        //! builtin functions for tables
        static std::map<std::string, BuiltinFunction> TableMembers =
        {
        };

    }
}
