
#include "private.h"
#include "builtin.h"

namespace Aqua
{
    using Value = Interpreter::Value;

    namespace Builtins
    {
        BUILTIN_PROTO(rtbuiltin_func_string_length, ip, self, args)
        {
            (void)ip;
            (void)args;
            Value::NumberType sz;
            sz = Value::NumberType(self.string().size());
            return Value(sz);
        }

        BUILTIN_PROTO(rtbuiltin_func_string_substr, ip, self, args)
        {
            (void)ip;
            (void)self;
            (void)args;
            Value::NumberType from;
            Value::NumberType to;
            //if(ip.needNumber(args[0], &from) && ip.needNumber(args[1], &to));
            std::cerr << "<string>.substr: args:" << std::endl;
            for(size_t i=0; i<args.size(); i++)
            {
                std::cerr << "  [" << i << "] = " << args[i] << std::endl;
            }
            return Value();
        }
    }
}
