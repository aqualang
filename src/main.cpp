
#include "private.h"

using namespace Aqua;

bool read_file(const std::string& path, std::string* dest)
{
    std::fstream in;
    in.open(path, std::ios::in);
    *dest = std::string((std::istreambuf_iterator<char>(in)), (std::istreambuf_iterator<char>()));
    return true;
}

void make_argv(Interpreter& ip, size_t start, std::vector<std::string>& args)
{
    size_t i;
    auto arr = ip.makeArray();
    for(i=start; i<args.size(); i++)
    {
        arr->add(ip.makeString(args[i]));
    }
    ip.setGlobal(ip.makeString("ARGV"), arr);
}

int interp_main(std::vector<std::string>& args)
{
    std::string fname;
    std::string source;
    Interpreter ip;
    if(args.size() > 1)
    {
        fname = args[1];
        if(fname == "-e")
        {
            source = args[2];
            args[2] = "<commandline>";
            make_argv(ip, 2, args);
            ip.run(source);
            return 0;
        }
        else
        {
            if(read_file(fname, &source))
            {
                make_argv(ip, 1, args);
                ip.run(source);
                return 0;
            }
            else
            {
                std::cerr << "cannot open '" << fname << "' for reading" << std::endl;
                return 1;
            }
        }
    }
    else
    {
        std::cerr << "no input file provided" << std::endl;
        return 1;
    }
    return 0;
}

int main(int argc, char *argv[])
{
    std::vector<std::string> args(argv, argv + argc);
    return interp_main(args);
}
