
namespace Aqua
{
    namespace Internal
    {
        static const std::unordered_map<std::string, TokenType> keywords =
        {
            {"if",               Tok_If},
            {"else",             Tok_Else},
            {"while",            Tok_While},
            {"for",              Tok_For},
            {"return",           Tok_Return},
            {"break",            Tok_Break},
            {"continue",         Tok_Continue},
            {"func",             Tok_Function},
            {"nil",              Tok_Nil},
            {"expression",       Tok_Expression},
            {"statement",        Tok_Statement},
            {"eval",             Tok_Eval},
            {"parse_statement",  Tok_ParseStatement},
            {"parse_expression", Tok_ParseExpression},
            {"global",           Tok_Global},
            {"local",            Tok_Local},
            {"root",             Tok_Root},
            {"typeof",           Tok_TypeOf},
            {"sizeof",           Tok_SizeOf},

            // these aren't implemented yet
            {"switch",           Tok_Switch},
            {"case",             Tok_Case},
            {"import",           Tok_Import},
        };

        template<class Iter>
        std::shared_ptr<std::vector<Token<Iter>>> tokenize(Iter begin, Iter end)
        {
            auto tokens_ptr = std::make_shared<std::vector<Token<Iter>>>();
            std::vector<Token<Iter>> &tokens = *tokens_ptr;
            Iter i = begin;
            while(i!=end)
            {
                Iter start = i;
                switch(*i)
                {
                    case ' ':
                    case '\t':
                    case '\n': // ignore whitespaces
                        ++i;
                        break;
                    case '(':
                        tokens.emplace_back(Tok_LeftParen, i, i+1);
                        ++i;
                        break;
                    case ')':
                        tokens.emplace_back(Tok_RightParen, i, i+1);
                        ++i;
                        break;
                    case '[':
                        tokens.emplace_back(Tok_LeftBracket, i, i+1);
                        ++i;
                        break;
                    case ']':
                        tokens.emplace_back(Tok_RightBracket, i, i+1);
                        ++i;
                        break;
                    case '{':
                        tokens.emplace_back(Tok_LeftBrace, i, i+1);
                        ++i;
                        break;
                    case '}':
                        tokens.emplace_back(Tok_RightBrace, i, i+1);
                        ++i;
                        break;
                    case ',':
                        tokens.emplace_back(Tok_Comma, i, i+1);
                        ++i;
                        break;
                    case '.':
                        tokens.emplace_back(Tok_Dot, i, i+1);
                        ++i;
                        break;
                    case ';':
                        tokens.emplace_back(Tok_Semicolon, i, i+1);
                        ++i;
                        break;
                    case ':':
                        tokens.emplace_back(Tok_Colon, i, i+1);
                        ++i;
                        break;
                    case '+':
                        if(i+1 != end && *(i+1) == '=')
                        {
                            tokens.emplace_back(Tok_PlusEqual, i, i+2);
                            i+=2;
                        }
                        else
                        {
                            tokens.emplace_back(Tok_Plus, i, i+1);
                            ++i;
                        }
                        break;
                    case '-':
                        if(i+1 != end && *(i+1) == '=')
                        {
                            tokens.emplace_back(Tok_DashEqual, i, i+2);
                            i+=2;
                        }
                        else
                        {
                            tokens.emplace_back(Tok_Dash, i, i+1);
                            ++i;
                        }
                        break;
                    case '*':
                        if(i+1 != end && *(i+1) == '=')
                        {
                            tokens.emplace_back(Tok_StarEqual, i, i+2);
                            i+=2;
                        }
                        else
                        {
                            tokens.emplace_back(Tok_Star, i, i+1);
                            ++i;
                        }
                        break;
                    case '%':
                        if(i+1 != end && *(i+1) == '=')
                        {
                            tokens.emplace_back(Tok_PercentEqual, i, i+2);
                            i+=2;
                        }
                        else
                        {
                            tokens.emplace_back(Tok_Percent, i, i+1);
                            ++i;
                        }
                        break;
                    case '/':
                        if(i+1 != end && *(i+1) == '/')
                        {
                            ++i;
                            while(i!=end && *i != '\n')
                            {
                                ++i;
                            } ++i;
                        }
                        else if(i+1 != end && *(i+1) == '=')
                        {
                            tokens.emplace_back(Tok_SlashEqual, i, i+2);
                            i+=2;
                        }
                        else
                        {
                            tokens.emplace_back(Tok_Slash, i, i+1);
                            ++i;
                        }
                        break;
                    case '<':
                        if(i+1 != end && *(i+1) == '=')
                        {
                            tokens.emplace_back(Tok_LessEqual, i, i+2);
                            i+=2;
                        }
                        else
                        {
                            tokens.emplace_back(Tok_Less, i, i+1);
                            ++i;
                        }
                        break;
                    case '>':
                        if(i+1 != end && *(i+1) == '=')
                        {
                            tokens.emplace_back(Tok_GreaterEqual, i, i+2);
                            i+=2;
                        }
                        else
                        {
                            tokens.emplace_back(Tok_Greater, i, i+1);
                            ++i;
                        }
                        break;
                    case '=':
                        if(i+1 != end && *(i+1) == '=')
                        {
                            tokens.emplace_back(Tok_EqOp, i, i+2);
                            i+=2;
                        }
                        else
                        {
                            tokens.emplace_back(Tok_Equal, i, i+1);
                            ++i;
                        }
                        break;
                    case '!':
                        if(i+1 != end && *(i+1) == '=')
                        {
                            tokens.emplace_back(Tok_NotEqual, i, i+2);
                            i+=2;
                        }
                        else
                        {
                            tokens.emplace_back(Tok_Not, i, i+1);
                            i+=2;
                        }
                        break;
                    case '&':
                        if(i+1 != end && *(i+1) == '&')
                        {
                            tokens.emplace_back(Tok_LogicalAnd, i, i+2);
                            i+=2;
                        }
                        else
                        {
                            auto lc = Utils::LineColumn(begin, i);
                            throw std::runtime_error(
                                std::to_string(lc.first)  + ":" +
                                std::to_string(lc.second) + ": unrecognized symbol '" + *i + "', expected '&'"
                            );
                        }
                        break;
                    case '|':
                        if(i+1 != end && *(i+1) == '|')
                        {
                            tokens.emplace_back(Tok_LogicalOr, i, i+2);
                            i+=2;
                        }
                        else
                        {
                            auto lc = Utils::LineColumn(begin, i);
                            throw std::runtime_error(
                                std::to_string(lc.first)  + ":" +
                                std::to_string(lc.second) + ": unrecognized symbol '" + *i + "', expected '|'"
                            );
                        }
                        break;
                    case '"':
                        ++i;
                        while(i!=end && !(*i == '"' && *(i-1) != '\\'))
                        {
                            ++i;
                        }
                        ++i;
                        tokens.emplace_back(Tok_String, start, i);
                        break;
                    default:
                        if(std::isdigit(*i))
                        {
                            while(i!=end && std::isdigit(*i))
                            {
                                ++i;
                            }
                            if(i!=end && (*i == '.'))
                            {
                                ++i;
                                while((i != end) && std::isdigit(*i))
                                {
                                    ++i;
                                }
                            }
                            if((i != end) && (*i == 'e'))
                            {
                                ++i;
                                if((i != end) && (*i == '-'))
                                {
                                    ++i;
                                }
                                while(i!=end && std::isdigit(*i))
                                {
                                    ++i;
                                }
                            }
                            tokens.emplace_back(Tok_Number, start, i);
                            break;
                        }
                        else if(std::isalpha(*i) || (*i == '_') || (*i == '$'))
                        {
                            ++i;
                            while(i!=end && (std::isalnum(*i) || *i == '_'))
                            {
                                ++i;
                            }
                            auto kw = keywords.find(std::string(start, i));
                            if(kw == keywords.end())
                            {
                                tokens.emplace_back(Tok_Identifier, start, i);
                            }
                            else
                            {
                                tokens.emplace_back(kw->second, start, i);
                            }
                            break;
                        }
                        else
                        {
                            auto lc = Utils::LineColumn(begin, i);
                            throw std::runtime_error(
                                std::to_string(lc.first)  + ":" +
                                std::to_string(lc.second) + ": unrecognized char '" + *i + "'"
                            );
                        }
                }
            }
            return tokens_ptr;
        }

        class Parser
        {
            public:
                using FuncType = Interpreter;
                using Value    = Interpreter::Value;
                using Iter     = std::vector<Token<std::string::iterator>>::iterator;
                using return_t = std::shared_ptr<ASTNode<Iter,Value,FuncType>>;

                template<typename StrIter>
                return_t operator()(StrIter sbegin, StrIter send)
                {
                    auto source = std::make_shared<std::string>(sbegin, send);
                    auto tokens = tokenize(source->begin(), source->end());
                    this->begin = tokens->begin();
                    this->end = tokens->end();
                    i = this->begin;
                    accepted = this->end;
                    auto node = statement_list();
                    node->source = source;
                    node->tokens = tokens;
                    node->root = node;
                    node->inject_dependencies();
                    return node;
                }

                template<class StrIter>
                return_t parse_expression(StrIter sbegin, StrIter send)
                {
                    auto source = std::make_shared<std::string>(sbegin, send);
                    auto tokens = tokenize(source->begin(), source->end());
                    this->begin = tokens->begin();
                    this->end = tokens->end();
                    i = this->begin;
                    accepted = this->end;
                    auto node = expression();
                    node->source = source;
                    node->tokens = tokens;
                    node->root = node;
                    node->inject_dependencies();
                    return node;
                }

            private:
                Iter i;
                Iter accepted;

            public:
                Iter begin;
                Iter end;

            public:
                void advance();

                void rewind(Iter pos);

                bool peek(TokenType t, int amount = 0);

                bool accept(TokenType t);

                bool end_of_input();

                bool expect(TokenType t);

                void expect(const std::string &expected);

                return_t variable();

                return_t field_name();

                return_t identifier_list();

                return_t function();

                return_t parse_expression();

                return_t parse_statement();

                return_t tree();

                return_t root();

                return_t eval();

                return_t type();

                return_t size();

                return_t do_import();

                return_t table_initializer();

                return_t array_initializer();

                return_t primary_expression();

                return_t postfix_expression();

                return_t unary_expression();

                return_t multiplicative_expression();

                return_t additive_expression();

                return_t relational_expression();

                return_t equality_expression();

                return_t logical_and_expression();

                return_t logical_or_expression();

                return_t assignment_expression();

                return_t initializer_assignment_expression();

                return_t expression();

                return_t block();

                return_t if_statement();

                return_t while_statement();

                return_t nop();

                return_t for_statement();

                return_t break_statement();

                return_t continue_statement();

                return_t return_statement();

                return_t statement();

                return_t statement_list();
        };
    }
}
