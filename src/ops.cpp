
#include "private.h"

namespace Aqua
{
    using namespace Internal;

    Interpreter::Value Interpreter::operator()(Variable<Iter,Interpreter::Value,FuncType> &node)
    {
        return get(node.name);
    }

    Interpreter::Value Interpreter::operator()(FieldName<Iter,Interpreter::Value,FuncType> &node)
    {
        return node.name;
    }

    Interpreter::Value Interpreter::operator()(Constant<Iter,Interpreter::Value,FuncType> &node)
    {
        return node.value;
    }

    Interpreter::Value Interpreter::operator()(Nop<Iter,Interpreter::Value,FuncType>&)
    {
        return Interpreter::Value(1.0);
    }

    Interpreter::Value Interpreter::operator()(Nil<Iter,Interpreter::Value,FuncType>&)
    {
        return Interpreter::Value();
    }

    Interpreter::Value Interpreter::operator()(String<Iter,Interpreter::Value,FuncType> &node)
    {
        return node.value;
    }

    Interpreter::Value Interpreter::operator()(Parens<Iter,Interpreter::Value,FuncType> &node)
    {
        return node.children[0]->accept(*this);
    }


    Interpreter::Value Interpreter::operator()(TypeOf<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value expr;
        expr = node.children[0]->accept(*this);
        switch(expr.type())
        {
            case Interpreter::Value::Type::String:
                return Interpreter::Value(std::string("string"));
            case Interpreter::Value::Type::Number:
                return Interpreter::Value(std::string("number"));
            case Interpreter::Value::Type::Tree:
                return Interpreter::Value(std::string("tree"));
            case Interpreter::Value::Type::Function:
                return Interpreter::Value(std::string("function"));
            case Interpreter::Value::Type::Table:
                {
                    #if 0
                    Interpreter::Value funcval;
                    funcval = expr.table()->get(makeString("__typeof"));
                    if(funcval.type() == Interpreter::Value::Type::Function)
                    {
                        std::vector<Interpreter::Value> stack;
                        //std::cerr << "table has '__typeof'" << std::endl;
                        //func.function()(*this, stack);
                        auto func = funcval.function()->accept(*this);
                        //func(*this, stack);
                        (*this)(func);
                        return stack[0];
                    }
                    #endif
                    return Interpreter::Value(std::string("table"));
                }
            case Interpreter::Value::Type::Array:
                return Interpreter::Value(std::string("array"));
            case Interpreter::Value::Type::Nil:
                return Interpreter::Value(std::string("nil"));
            default:
                error("unknown type", node);
                return Interpreter::Value();
        }
    }

    Interpreter::Value Interpreter::operator()(SizeOf<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value expr;
        expr = node.children[0]->accept(*this);
        switch(expr.type())
        {
            case Interpreter::Value::Type::String:
                return Interpreter::Value(expr.string().size());
            case Interpreter::Value::Type::Number:
                return Interpreter::Value(1);
            case Interpreter::Value::Type::Tree:
                return Interpreter::Value(expr.pointer()->size());
            case Interpreter::Value::Type::Function:
                return Interpreter::Value(expr.pointer()->size());
            case Interpreter::Value::Type::Table:
                return Interpreter::Value(expr.table()->size());
            case Interpreter::Value::Type::Array:
                return Interpreter::Value(expr.array()->size());
            case Interpreter::Value::Type::Nil:
                return Interpreter::Value(0.0);
            default:
                error("unknown type", node);
                return Interpreter::Value();
        }
    }

    Interpreter::Value Interpreter::operator()(FieldAccess<Iter,Interpreter::Value,FuncType> &node)
    {
        int idx;
        Interpreter::Value left;
        Interpreter::Value right;
        left = node.children[0]->accept(*this);
        if(left.type() == Interpreter::Value::Type::Table)
        {
            right = node.children[1]->accept(*this);
            return left.table()->get(right);
        }
        else if(left.type() == Interpreter::Value::Type::Array)
        {
            right = node.children[1]->accept(*this);
            if(right.type() != Interpreter::Value::Type::Number || !isint(right.number()))
            {
                error("array index not a integer", node);
            }
            idx = right.number();
            if(idx<0 || idx>=int(left.array()->size()))
            {
                error("array index out of range", node);
            }
            return left.array()->get(right);
        }
        else if(left.type() == Interpreter::Value::Type::String)
        {
            auto selfstr = left.string();
            right = node.children[1]->accept(*this);
            if(right.type() != Interpreter::Value::Type::Number || !isint(right.number()))
            {
                error("string index not an integer", node);
            }
            idx = right.number();
            if((idx < 0) || (idx >= int(selfstr.size())))
            {
                error("string index out of range", node);
            }
            return Value(Value::NumberType(selfstr.at(idx)));
        }
        else
        {
            error("can only index tables or arrays", node);
            return Interpreter::Value();
        }
    }

    Interpreter::Value Interpreter::operator()(FieldAssignment<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value table;
        table = node.children[0]->accept(*this);
        if(table.type() == Interpreter::Value::Type::Table)
        {
            Interpreter::Value index = node.children[1]->accept(*this);
            Interpreter::Value value = node.children[2]->accept(*this);
            table.table()->set(index, value);
            return value;
        }
        else if(table.type() == Interpreter::Value::Type::Array)
        {
            Interpreter::Value index = node.children[1]->accept(*this);
            if(index.type() != Interpreter::Value::Type::Number || !isint(index.number()))
            {
                error("array index not a integer.", node);
            }
            int idx = index.number();
            if(idx<0 || idx>=int(table.array()->size()))
            {
                error("array index out of range.", node);
            }
            Interpreter::Value value = node.children[2]->accept(*this);
            table.array()->set(index, value);
            return value;
        }
        else
        {
            error("can only index tables or arrays.", node);
            return Interpreter::Value();
        }
    }

    Interpreter::Value Interpreter::operator()(AddFieldAssignment<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value table;
        Interpreter::Value index, left, right;
        Interpreter::Value::Type t;
        table = node.children[0]->accept(*this);
        t = table.type();
        if(t == Interpreter::Value::Type::Table)
        {
            index = node.children[1]->accept(*this);
            left = table.table()->get(index);
            right = node.children[2]->accept(*this);
        }
        else if(t == Interpreter::Value::Type::Array)
        {
            index = node.children[1]->accept(*this);
            if(index.type() != Interpreter::Value::Type::Number || !isint(index.number()))
            {
                error("array index not a integer.", node);
            }
            int idx = index.number();
            if(idx<0 || idx>=int(table.array()->size()))
            {
                error("array index out of range.", node);
            }
            left = table.array()->get(index);
            right = node.children[2]->accept(*this);
        }
        else
        {
            error("can only index tables and arrays.", node);
            return Interpreter::Value();
        }
        if(left.type() == Interpreter::Value::Type::Number && right.type() == Interpreter::Value::Type::Number)
        {
            left = Interpreter::Value(left.number() + right.number());
        }
        else if(left.type() == Interpreter::Value::Type::String && right.type() == Interpreter::Value::Type::String)
        {
            left.string() += right.string();
        }
        else if(left.type() == Interpreter::Value::Type::Table && right.type() == Interpreter::Value::Type::Table)
        {
            left.table()->append(*right.table());
        }
        else if(left.type() == Interpreter::Value::Type::Array && right.type() == Interpreter::Value::Type::Array)
        {
            left.array()->append(*right.array());
        }
        else
        {
            error("invalid operands '+='.", node);
            return Interpreter::Value();
        }
        if(t == Interpreter::Value::Type::Table)
        {
            table.table()->set(index, left);
        }
        else if(t == Interpreter::Value::Type::Array)
        {
            table.array()->set(index, left);
        }
        return left;
    }

    Interpreter::Value Interpreter::operator()(SubFieldAssignment<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value table = node.children[0]->accept(*this);
        Interpreter::Value index, left, right;
        Interpreter::Value::Type t = table.type();
        if(t == Interpreter::Value::Type::Table)
        {
            index = node.children[1]->accept(*this);
            left = table.table()->get(index);
            right = node.children[2]->accept(*this);
        }
        else if(t == Interpreter::Value::Type::Array)
        {
            index = node.children[1]->accept(*this);
            if(index.type() != Interpreter::Value::Type::Number || !isint(index.number()))
            {
                error("array index not a integer.", node);
            }
            int idx = index.number();
            if(idx<0 || idx>=int(table.array()->size()))
            {
                error("array index out of range.", node);
            }
            left = table.array()->get(index);
            right = node.children[2]->accept(*this);
        }
        else
        {
            error("can only index tables and arrays.", node);
            return Interpreter::Value();
        }
        if(left.type() == Interpreter::Value::Type::Number && right.type() == Interpreter::Value::Type::Number)
        {
            left = Interpreter::Value(left.number() - right.number());
        }
        else
        {
            error("invalid operands to '-='.", node);
            return Interpreter::Value();
        }
        if(t == Interpreter::Value::Type::Table)
        {
            table.table()->set(index, left);
        }
        else if(t == Interpreter::Value::Type::Array)
        {
            table.array()->set(index, left);
        }
        return left;
    }

    Interpreter::Value Interpreter::operator()(MulFieldAssignment<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value table = node.children[0]->accept(*this);
        Interpreter::Value index, left, right;
        Interpreter::Value::Type t = table.type();
        if(t == Interpreter::Value::Type::Table)
        {
            index = node.children[1]->accept(*this);
            left = table.table()->get(index);
            right = node.children[2]->accept(*this);
        }
        else if(t == Interpreter::Value::Type::Array)
        {
            index = node.children[1]->accept(*this);
            if(index.type() != Interpreter::Value::Type::Number || !isint(index.number()))
            {
                error("array index not a integer.", node);
            }
            int idx = index.number();
            if(idx<0 || idx>=int(table.array()->size()))
            {
                error("array index out of range.", node);
            }
            left = table.array()->get(index);
            right = node.children[2]->accept(*this);
        }
        else
        {
            error("can only index tables and arrays.", node);
            return Interpreter::Value();
        }
        if(left.type() == Interpreter::Value::Type::Number && right.type() == Interpreter::Value::Type::Number)
        {
            left = Interpreter::Value(left.number() * right.number());
        }
        else
        {
            error("invalid operands to '*='.", node);
            return Interpreter::Value();
        }
        if(t == Interpreter::Value::Type::Table)
        {
            table.table()->set(index, left);
        }
        else if(t == Interpreter::Value::Type::Array)
        {
            table.array()->set(index, left);
        }
        return left;
    }

    Interpreter::Value Interpreter::operator()(DivFieldAssignment<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value table = node.children[0]->accept(*this);
        Interpreter::Value index, left, right;
        Interpreter::Value::Type t = table.type();
        if(t == Interpreter::Value::Type::Table)
        {
            index = node.children[1]->accept(*this);
            left = table.table()->get(index);
            right = node.children[2]->accept(*this);
        }
        else if(t == Interpreter::Value::Type::Array)
        {
            index = node.children[1]->accept(*this);
            if(index.type() != Interpreter::Value::Type::Number || !isint(index.number()))
            {
                error("array index not a integer.", node);
            }
            int idx = index.number();
            if(idx<0 || idx>=int(table.array()->size()))
            {
                error("array index out of range.", node);
            }
            left = table.array()->get(index);
            right = node.children[2]->accept(*this);
        }
        else
        {
            error("can only index tables and arrays.", node);
            return Interpreter::Value();
        }
        if(left.type() == Interpreter::Value::Type::Number && right.type() == Interpreter::Value::Type::Number)
        {
            left = Interpreter::Value(left.number() / right.number());
        }
        else
        {
            error("invalid operands to '/='.", node);
            return Interpreter::Value();
        }
        if(t == Interpreter::Value::Type::Table)
        {
            table.table()->set(index, left);
        }
        else if(t == Interpreter::Value::Type::Array)
        {
            table.array()->set(index, left);
        }
        return left;
    }

    Interpreter::Value Interpreter::operator()(ModFieldAssignment<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value table = node.children[0]->accept(*this);
        Interpreter::Value index, left, right;
        Interpreter::Value::Type t = table.type();
        if(t == Interpreter::Value::Type::Table)
        {
            index = node.children[1]->accept(*this);
            left = table.table()->get(index);
            right = node.children[2]->accept(*this);
        }
        else if(t == Interpreter::Value::Type::Array)
        {
            index = node.children[1]->accept(*this);
            if(index.type() != Interpreter::Value::Type::Number || !isint(index.number()))
            {
                error("array index not a integer.", node);
            }
            int idx = index.number();
            if(idx<0 || idx>=int(table.array()->size()))
            {
                error("array index out of range.", node);
            }
            left = table.array()->get(index);
            right = node.children[2]->accept(*this);
        }
        else
        {
            error("can only index tables and arrays.", node);
            return Interpreter::Value();
        }
        if(left.type() == Interpreter::Value::Type::Number && right.type() == Interpreter::Value::Type::Number)
        {
            left = Interpreter::Value(std::fmod(left.number(), right.number()));
        }
        else
        {
            error("invalid operands to '%='.", node);
            return Interpreter::Value();
        }
        if(t == Interpreter::Value::Type::Table)
        {
            table.table()->set(index, left);
        }
        else if(t == Interpreter::Value::Type::Array)
        {
            table.array()->set(index, left);
        }
        return left;
    }

    Interpreter::Value Interpreter::operator()(UnaryPlusExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value operand = node.children[0]->accept(*this);
        if(operand.type() != Interpreter::Value::Type::Number)
        {
            error("invalid operand to '+'.", node);
        }
        return operand;
    }

    Interpreter::Value Interpreter::operator()(UnaryMinusExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value operand = node.children[0]->accept(*this);
        if(operand.type() != Interpreter::Value::Type::Number)
        {
            error("invalid operand to '-'.", node);
        }
        return Interpreter::Value(-operand.number());
    }

    Interpreter::Value Interpreter::operator()(UnaryNotExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value operand = node.children[0]->accept(*this);
        if(operand.type() != Interpreter::Value::Type::Number)
        {
            error("invalid operand to '!'.", node);
        }
        return Interpreter::Value(operand.number()==0.0?1.0:0.0);
    }

    Interpreter::Value Interpreter::operator()(MultiplyExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value left = node.children[0]->accept(*this);
        Interpreter::Value right = node.children[1]->accept(*this);
        if(left.type() == Interpreter::Value::Type::Number && right.type() == Interpreter::Value::Type::Number)
        {
            return Interpreter::Value(left.number() * right.number());
        }
        else
        {
            error("invalid operands to '*'.", node);
            return Interpreter::Value();
        }
    }

    Interpreter::Value Interpreter::operator()(DivideExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value left = node.children[0]->accept(*this);
        Interpreter::Value right = node.children[1]->accept(*this);
        if(left.type() == Interpreter::Value::Type::Number && right.type() == Interpreter::Value::Type::Number)
        {
            return Interpreter::Value(left.number() / right.number());
        }
        else
        {
            error("invalid operands to '/'.", node);
            return Interpreter::Value();
        }
    }

    Interpreter::Value Interpreter::operator()(ModuloExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value left = node.children[0]->accept(*this);
        Interpreter::Value right = node.children[1]->accept(*this);
        if(left.type() == Interpreter::Value::Type::Number && right.type() == Interpreter::Value::Type::Number)
        {
            return Interpreter::Value(std::fmod(left.number(), right.number()));
        }
        else
        {
            error("invalid operands to '%'.", node);
            return Interpreter::Value();
        }
    }

    Interpreter::Value Interpreter::operator()(AddExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value left = node.children[0]->accept(*this);
        Interpreter::Value right = node.children[1]->accept(*this);
        if(left.type() == Interpreter::Value::Type::Number && right.type() == Interpreter::Value::Type::Number)
        {
            return Interpreter::Value(left.number() + right.number());
        }
        else if(left.type() == Interpreter::Value::Type::String && right.type() == Interpreter::Value::Type::String)
        {
            return Interpreter::Value(left.string() + right.string());
        }
        else if(left.type() == Interpreter::Value::Type::Table && right.type() == Interpreter::Value::Type::Table)
        {
            typename Interpreter::Value::TableContainer table(new Table<Interpreter::Value>);
            table->append(*left.table());
            table->append(*right.table());
            return Interpreter::Value(table);
        }
        else if(left.type() == Interpreter::Value::Type::Array && right.type() == Interpreter::Value::Type::Array)
        {
            typename Interpreter::Value::ArrayContainer array(new Array<Interpreter::Value>);
            array->append(*left.array());
            array->append(*right.array());
            return Interpreter::Value(array);
        }
        else
        {
            error("invalid operands to '+'.", node);
            return Interpreter::Value();
        }
    }

    Interpreter::Value Interpreter::operator()(SubtractExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value left = node.children[0]->accept(*this);
        Interpreter::Value right = node.children[1]->accept(*this);
        if(left.type() == Interpreter::Value::Type::Number && right.type() == Interpreter::Value::Type::Number)
        {
            return Interpreter::Value(left.number() - right.number());
        }
        else
        {
            error("invalid operands to '-'.", node);
            return Interpreter::Value();
        }
    }

    Interpreter::Value Interpreter::operator()(LessExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value left = node.children[0]->accept(*this);
        Interpreter::Value right = node.children[1]->accept(*this);
        if(left.type() == Interpreter::Value::Type::Number && right.type() == Interpreter::Value::Type::Number)
        {
            return Interpreter::Value(left.number() < right.number());
        }
        else if(left.type() == Interpreter::Value::Type::String && right.type() == Interpreter::Value::Type::String)
        {
            return Interpreter::Value(left.string() < right.string());
        }
        else
        {
            error("invalid operands to '<'.", node);
            return Interpreter::Value();
        }
    }

    Interpreter::Value Interpreter::operator()(LessEqualExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value left = node.children[0]->accept(*this);
        Interpreter::Value right = node.children[1]->accept(*this);
        if(left.type() == Interpreter::Value::Type::Number && right.type() == Interpreter::Value::Type::Number)
        {
            return Interpreter::Value(left.number() <= right.number());
        }
        else if(left.type() == Interpreter::Value::Type::String && right.type() == Interpreter::Value::Type::String)
        {
            return Interpreter::Value(left.string() <= right.string());
        }
        else
        {
            error("invalid operands to '<='.", node);
            return Interpreter::Value();
        }
    }

    Interpreter::Value Interpreter::operator()(GreaterExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value left = node.children[0]->accept(*this);
        Interpreter::Value right = node.children[1]->accept(*this);
        if(left.type() == Interpreter::Value::Type::Number && right.type() == Interpreter::Value::Type::Number)
        {
            return Interpreter::Value(left.number() > right.number());
        }
        else if(left.type() == Interpreter::Value::Type::String && right.type() == Interpreter::Value::Type::String)
        {
            return Interpreter::Value(left.string() > right.string());
        }
        else
        {
            error("invalid operands to '>'.", node);
            return Interpreter::Value();
        }
    }

    Interpreter::Value Interpreter::operator()(GreaterEqualExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value left = node.children[0]->accept(*this);
        Interpreter::Value right = node.children[1]->accept(*this);
        if(left.type() == Interpreter::Value::Type::Number && right.type() == Interpreter::Value::Type::Number)
        {
            return Interpreter::Value(left.number() >= right.number());
        }
        else if(left.type() == Interpreter::Value::Type::String && right.type() == Interpreter::Value::Type::String)
        {
            return Interpreter::Value(left.string() >= right.string());
        }
        else
        {
            error("invalid operands to '>='.", node);
            return Interpreter::Value();
        }
    }

    Interpreter::Value Interpreter::operator()(EqualExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value left = node.children[0]->accept(*this);
        Interpreter::Value right = node.children[1]->accept(*this);
        return Interpreter::Value(left == right);
    }

    Interpreter::Value Interpreter::operator()(NotEqualExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value left = node.children[0]->accept(*this);
        Interpreter::Value right = node.children[1]->accept(*this);
        return Interpreter::Value(!(left == right));
    }

    Interpreter::Value Interpreter::operator()(LogicalAndExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value left = node.children[0]->accept(*this);
        if(left.type() == Interpreter::Value::Type::Number)
        {
            if(left.number() != 0.0)
            {
                Interpreter::Value right = node.children[1]->accept(*this);
                if(right.type() == Interpreter::Value::Type::Number)
                {
                    return Interpreter::Value(right.number() != 0.0);
                }
                else
                {
                    error("invalid operands to '&&'.", node);
                    return Interpreter::Value();
                }
            }
            else
            {
                return Interpreter::Value(0.0);
            }
        }
        else
        {
            error("invalid operands to '&&'.", node);
            return Interpreter::Value();
        }
    }

    Interpreter::Value Interpreter::operator()(LogicalOrExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value left = node.children[0]->accept(*this);
        if(left.type() == Interpreter::Value::Type::Number)
        {
            if(left.number() == 0.0)
            {
                Interpreter::Value right = node.children[1]->accept(*this);
                if(right.type() == Interpreter::Value::Type::Number)
                {
                    return Interpreter::Value(right.number() != 0.0);
                }
                else
                {
                    error("invalid operands to '||'.", node);
                    return Interpreter::Value();
                }
            }
            else
            {
                return Interpreter::Value(1.0);
            }
        }
        else
        {
            error("invalid operands to '||'.", node);
            return Interpreter::Value();
        }
    }

    Interpreter::Value Interpreter::operator()(AssignmentExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value left = node.children[0]->accept(*this);
        Interpreter::Value right = node.children[1]->accept(*this);
        set(left, right);
        return right;
    }

    Interpreter::Value Interpreter::operator()(GlobalAssignmentExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value left = node.children[0]->accept(*this);
        Interpreter::Value right = node.children[1]->accept(*this);
        setGlobal(left, right);
        return right;
    }

    Interpreter::Value Interpreter::operator()(LocalAssignmentExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value left = node.children[0]->accept(*this);
        Interpreter::Value right = node.children[1]->accept(*this);
        setLocal(left, right);
        return right;
    }

    Interpreter::Value Interpreter::operator()(AddAssignmentExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value name = node.children[0]->accept(*this);
        Interpreter::Value left = get(name);
        Interpreter::Value right = node.children[1]->accept(*this);
        if(left.type() == Interpreter::Value::Type::Number && right.type() == Interpreter::Value::Type::Number)
        {
            left = Interpreter::Value(left.number() + right.number());
        }
        else if(left.type() == Interpreter::Value::Type::String && right.type() == Interpreter::Value::Type::String)
        {
            left.string() += right.string();
        }
        else if(left.type() == Interpreter::Value::Type::Table && right.type() == Interpreter::Value::Type::Table)
        {
            left.table()->append(*right.table());
        }
        else if(left.type() == Interpreter::Value::Type::Array && right.type() == Interpreter::Value::Type::Array)
        {
            left.array()->append(*right.array());
        }
        else
        {
            error("invalid operands to '+='.", node);
            return Interpreter::Value();
        }
        set(name, left);
        return left;
    }

    Interpreter::Value Interpreter::operator()(SubAssignmentExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value name = node.children[0]->accept(*this);
        Interpreter::Value left = get(name);
        Interpreter::Value right = node.children[1]->accept(*this);
        if(left.type() == Interpreter::Value::Type::Number && right.type() == Interpreter::Value::Type::Number)
        {
            left = Interpreter::Value(left.number() - right.number());
        }
        else
        {
            error("invalid operands to '-='.", node);
            return Interpreter::Value();
        }
        set(name, left);
        return left;
    }

    Interpreter::Value Interpreter::operator()(MulAssignmentExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value name = node.children[0]->accept(*this);
        Interpreter::Value left = get(name);
        Interpreter::Value right = node.children[1]->accept(*this);
        if(left.type() == Interpreter::Value::Type::Number && right.type() == Interpreter::Value::Type::Number)
        {
            left = Interpreter::Value(left.number() * right.number());
        }
        else
        {
            error("invalid operands to '*='.", node);
            return Interpreter::Value();
        }
        set(name, left);
        return left;
    }

    Interpreter::Value Interpreter::operator()(DivAssignmentExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value name = node.children[0]->accept(*this);
        Interpreter::Value left = get(name);
        Interpreter::Value right = node.children[1]->accept(*this);
        if(left.type() == Interpreter::Value::Type::Number && right.type() == Interpreter::Value::Type::Number)
        {
            left = Interpreter::Value(left.number() / right.number());
        }
        else
        {
            error("invalid operands to '/='.", node);
            return Interpreter::Value();
        }
        set(name, left);
        return left;
    }

    Interpreter::Value Interpreter::operator()(ModAssignmentExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value name = node.children[0]->accept(*this);
        Interpreter::Value left = get(name);
        Interpreter::Value right = node.children[1]->accept(*this);
        if(left.type() == Interpreter::Value::Type::Number && right.type() == Interpreter::Value::Type::Number)
        {
            left = Interpreter::Value(std::fmod(left.number(), right.number()));
        }
        else
        {
            error("invalid operands to '%='.", node);
            return Interpreter::Value();
        }
        set(name, left);
        return left;
    }

    Interpreter::Value Interpreter::operator()(Block<Iter,Interpreter::Value,FuncType> &node)
    {
        LocalScope localscope(*this);
        return node.children[0]->accept(*this);
    }

    Interpreter::Value Interpreter::operator()(TableInitializer<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value::NumberType current = 0;
        typename Interpreter::Value::TableContainer table(new Table<Interpreter::Value>);
        for(auto tmpchild : node.children)
        {
            auto child = static_cast<InitializerAssignmentExpression<Iter,Interpreter::Value,FuncType>*>(tmpchild.get());
            if(child->children.size() == 1)
            {
                table->set(Interpreter::Value(current++), child->children[0]->accept(*this));
            }
            else
            {
                Interpreter::Value index = child->children[0]->accept(*this);
                if(index.type() == Interpreter::Value::Type::Number && isint(index.number()))
                {
                    current = index.number()+1;
                }
                table->set(index, child->children[1]->accept(*this));
            }
        }
        return Interpreter::Value(table);
    }

    Interpreter::Value Interpreter::operator()(ArrayInitializer<Iter,Interpreter::Value,FuncType> &node)
    {
        typename Interpreter::Value::ArrayContainer array(new Array<Interpreter::Value>);
        for(auto& tmpchild : node.children)
        {
            array->add(tmpchild->accept(*this));
        }
        return Interpreter::Value(array);
    }

    Interpreter::Value Interpreter::operator()(Function<Iter,Interpreter::Value,FuncType> &node)
    {
        return Interpreter::Value(Interpreter::Value::Type::Function, node.shared_from_this());
    }

    Interpreter::Value Interpreter::operator()(IdentifierList<Iter,Interpreter::Value,FuncType> &node)
    {
        auto i = m_function_stack.begin();
        auto end = m_function_stack.end();
        set(Interpreter::Value("this"), *i++);
        for(auto child : node.children)
        {
            if(i==end)
            {
                break;
            }
            set(child->accept(*this), *i++);
        }
        m_function_stack.clear();
        return Interpreter::Value();
    }

    Interpreter::Value Interpreter::operator()(BuiltinFunction<Iter,Interpreter::Value,FuncType> &node)
    {
        node.function(*this, m_function_stack);
        return Interpreter::Value();
    }

    Interpreter::Value Interpreter::operator()(Call<Iter,Interpreter::Value,FuncType> &node)
    {
        std::vector<Interpreter::Value> args;
        Interpreter::Value fun = node.children[0]->accept(*this);
        if(fun.type() != Interpreter::Value::Type::Function)
        {
            error("can not call non-function", node);
            return Interpreter::Value();
        }
        auto function = fun.function();
        auto innerargs = makeArray();
        for(size_t i = 1; i<node.children.size(); ++i)
        {
            auto val = node.children[i]->accept(*this);
            args.push_back(val);
            innerargs->add(val);
        }
        m_function_stack.push_back(Interpreter::Value());
        for(size_t i = 1; i<node.children.size(); ++i)
        {
            m_function_stack.push_back(args[i-1]);
        }
        FunctionScope functionscope(this);
        setLocal(Value("$args"), innerargs);
        function->children[0]->accept(*this);
        function->children[1]->accept(*this);
        if(!m_function_stack.empty())
        {
            Interpreter::Value result = m_function_stack.back();
            m_function_stack.clear();
            return result;
        }
        else
        {
            return Interpreter::Value();
        }
    }

    Interpreter::Value Interpreter::operator()(Tree<Iter,Interpreter::Value,FuncType> &node)
    {
        return Interpreter::Value(Interpreter::Value::Type::Tree, node.children[0]);
    }

    Interpreter::Value Interpreter::operator()(Root<Iter,Interpreter::Value,FuncType> &node)
    {
        if(node.children.empty())
        {
            return Interpreter::Value(Interpreter::Value::Type::Tree, node.root.lock());
        }
        else
        {
            Interpreter::Value tree = node.children[0]->accept(*this);
            if(tree.type() == Interpreter::Value::Type::Tree || tree.type() == Interpreter::Value::Type::Function)
            {
                return Interpreter::Value(Interpreter::Value::Type::Tree, tree.pointer()->root.lock());
            }
            else
            {
                error("cannot find root of non-tree.", node);
                return Interpreter::Value();
            }
        }
    }
}
