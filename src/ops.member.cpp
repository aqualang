
#include <sstream>
#include "private.h"
#include "builtin.h"

namespace Aqua
{
    using namespace Internal;
    using namespace Builtins;
    using Value = Interpreter::Value;

    // this function is dangerously close to turning really awfully hacky!

    static std::string no_such_index(Value& self, Value& name)
    {
        std::stringstream strm;
        strm << "cannot find index '" << name << "' for type <" << self.typeName() << ">";
        return strm.str();
    }

    template<typename NodeType>
    static void getFunctionArgs(Interpreter& ip, NodeType& node, std::vector<Value>& args)
    {
        for(size_t i = 2; i<node.children.size(); i++)
        {
            args.push_back(node.children[i]->accept(ip));
        }
    }

    Interpreter::Value Interpreter::operator()(MemberCall<Iter,Interpreter::Value,FuncType> &node)
    {
        std::vector<Value> args;
        Value table;
        Value name;
        table = node.children[0]->accept(*this);
        name = node.children[1]->accept(*this);
        auto namestr = name.string();
        if(table.type() == Value::Type::Table)
        {
            // nop
        }
        else if((table.type() == Value::Type::String) && BUILTIN_EXISTS(String, namestr))
        {
            getFunctionArgs(*this, node, args);
            return BUILTIN_GET(String, namestr)(*this, table, args);
        }
        else if((table.type() == Value::Type::Array) && BUILTIN_EXISTS(Array, namestr))
        {
            getFunctionArgs(*this, node, args);
            return BUILTIN_GET(Array, namestr)(*this, table, args);
        }
        else if((table.type() == Value::Type::Number) && BUILTIN_EXISTS(Number, namestr))
        {
            getFunctionArgs(*this, node, args);
            return BUILTIN_GET(Number, namestr)(*this, table, args);
        }
        else
        {
            error(no_such_index(table, name), node);
            return Value();
        }
        Value fun = table.table()->get(name);
        if(fun.type() != Value::Type::Function)
        {
            error("can not call non-function", node);
            return Value();
        }
        auto function = fun.function();
        getFunctionArgs(*this, node, args);
        m_function_stack.push_back(table);
        for(size_t i = 2; i<node.children.size(); ++i)
        {
            m_function_stack.push_back(args[i-2]);
        }
        FunctionScope functionscope(this);
        function->children[0]->accept(*this);
        function->children[1]->accept(*this);
        if(!m_function_stack.empty())
        {
            Value result = m_function_stack.back();
            m_function_stack.clear();
            return result;
        }
        else
        {
            return Value();
        }
    }
}
