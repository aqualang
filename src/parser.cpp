
#include "private.h"
#include "parser.h"

namespace Aqua
{
    namespace Internal
    {
        void Parser::advance()
        {
            if(i==end)
            {
                Iter last = end-1;
                auto lc = Utils::LineColumn(begin->begin, last->end);
                throw std::runtime_error(
                    std::to_string(lc.first)  + ":" +
                    std::to_string(lc.second) + ": unexpected end of input at");
            }
            else
            {
                ++i;
            }
        }

        void Parser::rewind(Iter pos)
        {
            i = pos;
        }

        bool Parser::peek(TokenType t, int amount)
        {
            return (((i+amount) != end) && (((i+amount)->type) == t));
        }

        bool Parser::accept(TokenType t)
        {
            if(i != end && i->type == t)
            {
                accepted = i;
                advance();
                return true;
            }
            else
            {
                return false;
            }
        }

        bool Parser::end_of_input()
        {
            return (i == end);
        }

        bool Parser::expect(TokenType t)
        {
            if(accept(t))
            {
                return true;
            }
            else
            {
                i = std::min(i, end-1);
                auto lc = Utils::LineColumn(begin->begin, i->begin);
                throw std::runtime_error(
                    std::to_string(lc.first)  + ":" +
                    std::to_string(lc.second) + ": unexpected token '" +
                    std::string(i->begin, i->end) + "'"
                );
                return false;
            }
        }

        void Parser::expect(const std::string &expected)
        {
            i = std::min(i, end-1);
            auto lc = Utils::LineColumn(begin->begin, i->begin);
            throw std::runtime_error(
                std::to_string(lc.first)  + ":" +
                std::to_string(lc.second) + ": expected " +
                expected+ " but got '" + std::string(i->begin, i->end) + "'"
            );
        }

        Parser::return_t Parser::variable()
        {
            expect(Tok_Identifier);
            auto node = std::make_shared<Variable<Iter,Value,FuncType>>();
            node->begin = accepted;
            node->end = accepted+1;
            node->name = Value(accepted->begin, accepted->end);
            return std::move(node);
        }

        Parser::return_t Parser::field_name()
        {
            expect(Tok_Identifier);
            auto node = std::make_shared<FieldName<Iter,Value,FuncType>>();
            node->begin = accepted;
            node->end = accepted+1;
            node->name = Value(accepted->begin, accepted->end);
            return std::move(node);
        }

        Parser::return_t Parser::identifier_list()
        {
            auto node = std::make_shared<IdentifierList<Iter,Value,FuncType>>();
            node->begin = i;
            expect(Tok_LeftParen);
            while(!accept(Tok_RightParen))
            {
                node->children.push_back(field_name());
                if(!peek(Tok_RightParen))
                {
                    accept(Tok_Comma);
                }
            }
            node->end = accepted+1;
            return std::move(node);
        }

        Parser::return_t Parser::function()
        {
            auto node = std::make_shared<Function<Iter,Value,FuncType>>();
            node->begin = i;
            expect(Tok_Function);
            node->children[0] = identifier_list();
            node->children[1] = statement();
            node->end = accepted+1;
            return std::move(node);
        }

        Parser::return_t Parser::parse_expression()
        {
            auto node = std::make_shared<ParseExpression<Iter,Value,FuncType>>();
            node->begin = i;
            expect(Tok_ParseExpression);
            expect(Tok_LeftParen);
            node->children[0] = expression();
            expect(Tok_RightParen);
            node->end = accepted+1;
            return std::move(node);
        }

        Parser::return_t Parser::parse_statement()
        {
            auto node = std::make_shared<ParseStatement<Iter,Value,FuncType>>();
            node->begin = i;
            expect(Tok_ParseStatement);
            expect(Tok_LeftParen);
            node->children[0] = expression();
            expect(Tok_RightParen);
            node->end = accepted+1;
            return std::move(node);
        }

        Parser::return_t Parser::tree()
        {
            auto node = std::make_shared<Tree<Iter,Value,FuncType>>();
            node->begin = i;
            if(accept(Tok_Expression))
            {
                expect(Tok_LeftParen);
                node->children[0] = expression();
                expect(Tok_RightParen);
            }
            else
            {
                expect(Tok_Statement);
                expect(Tok_LeftParen);
                node->children[0] = statement();
                expect(Tok_RightParen);
            }
            node->end = accepted+1;
            return std::move(node);
        }

        Parser::return_t Parser::root()
        {
            auto node = std::make_shared<Root<Iter,Value,FuncType>>();
            node->begin = i;
            expect(Tok_Root);
            if(accept(Tok_LeftParen))
            {
                node->children.push_back(expression());
                expect(Tok_RightParen);
            }
            node->end = accepted+1;
            return std::move(node);
        }

        Parser::return_t Parser::eval()
        {
            auto node = std::make_shared<Eval<Iter,Value,FuncType>>();
            node->begin = i;
            expect(Tok_Eval);
            expect(Tok_LeftParen);
            node->children[0] = unary_expression();
            expect(Tok_RightParen);
            node->end = accepted+1;
            return std::move(node);
        }

        Parser::return_t Parser::type()
        {
            auto node = std::make_shared<TypeOf<Iter,Value,FuncType>>();
            node->begin = i;
            expect(Tok_TypeOf);
            if(accept(Tok_LeftParen))
            {
                node->children[0] = expression();
                expect(Tok_RightParen);
            }
            node->end = accepted+1;
            return std::move(node);
        }

        Parser::return_t Parser::size()
        {
            auto node = std::make_shared<SizeOf<Iter,Value,FuncType>>();
            node->begin = i;
            expect(Tok_SizeOf);
            if(accept(Tok_LeftParen))
            {
                node->children[0] = expression();
                expect(Tok_RightParen);
            }
            node->end = accepted+1;
            return std::move(node);
        }

        Parser::return_t Parser::do_import()
        {
            auto node = std::make_shared<DoImport<Iter,Value,FuncType>>();
            node->begin = i;
            expect(Tok_Import);
            if(accept(Tok_LeftParen))
            {
                node->children[0] = expression();
                expect(Tok_RightParen);
            }
            node->end = accepted+1;
            return std::move(node);
        }

        Parser::return_t Parser::table_initializer()
        {
            auto node = std::make_shared<TableInitializer<Iter,Value,FuncType>>();
            node->begin = i;
            expect(Tok_LeftBrace);
            while(!accept(Tok_RightBrace))
            {
                node->children.push_back(initializer_assignment_expression());
                if(!peek(Tok_RightBrace))
                {
                    accept(Tok_Comma);
                }
            }
            node->end = accepted+1;
            return std::move(node);
        }

        Parser::return_t Parser::array_initializer()
        {
            auto node = std::make_shared<ArrayInitializer<Iter,Value,FuncType>>();
            node->begin = i;
            expect(Tok_LeftBracket);
            while(!accept(Tok_RightBracket))
            {
                node->children.push_back(logical_or_expression());
                if(!peek(Tok_RightBracket))
                {
                    accept(Tok_Comma);
                }
            }
            node->end = accepted+1;
            return std::move(node);
        }

        Parser::return_t Parser::primary_expression()
        {
            if (peek(Tok_Identifier) || peek(Tok_Global) || peek(Tok_Local))
            {
                return variable();
            }
            else if (accept(Tok_Number))
            {
                auto node = std::make_shared<Constant<Iter,Value,FuncType>>();
                node->begin = accepted;
                node->end = accepted+1;
                node->value = Value(std::stod(std::string(accepted->begin, accepted->end)));
                return std::move(node);
            }
            else if (accept(Tok_Nil))
            {
                auto node = std::make_shared<Nil<Iter,Value,FuncType>>();
                node->begin = accepted;
                node->end = accepted+1;
                return std::move(node);
            }
            else if (accept(Tok_String))
            {
                auto node = std::make_shared<String<Iter,Value,FuncType>>();
                node->begin = accepted;
                node->end = accepted+1;
                node->value = Value(Utils::Unescape(std::string(accepted->begin+1, accepted->end-1)));
                return std::move(node);
            }
            else if (accept(Tok_LeftParen))
            {
                auto node = std::make_shared<Parens<Iter,Value,FuncType>>();
                node->begin = accepted;
                node->children[0] = expression();
                expect(Tok_RightParen);
                node->end = accepted+1;
                return std::move(node);
            }
            else if (peek(Tok_LeftBrace))
            {
                return table_initializer();
            }
            else if (peek(Tok_LeftBracket))
            {
                return array_initializer();
            }
            else if (peek(Tok_Expression) || peek(Tok_Statement))
            {
                return tree();
            }
            else if (peek(Tok_Root))
            {
                return root();
            }
            else if (peek(Tok_TypeOf))
            {
                return type();
            }
            else if (peek(Tok_Eval))
            {
                return eval();
            }
            else if (peek(Tok_SizeOf))
            {
                return size();
            }
            else if(peek(Tok_Import))
            {
                return do_import();
            }
            else if (peek(Tok_ParseExpression))
            {
                return parse_expression();
            }
            else if (peek(Tok_ParseStatement))
            {
                return parse_statement();
            }
            else if (peek(Tok_Function))
            {
                return function();
            }
            else if(peek(Tok_If))
            {
                return if_statement();
            }
            else
            {
                expect("primary expression");
                return nullptr;
            }
        }

        Parser::return_t Parser::postfix_expression()
        {
            Iter from = i;
            auto left = primary_expression();
            while(true)
            {
                if(accept(Tok_LeftBracket))
                {
                    auto right = expression();
                    expect(Tok_RightBracket);
                    if(peek(Tok_Equal) || peek(Tok_PlusEqual) || peek(Tok_DashEqual) ||
                            peek(Tok_StarEqual) || peek(Tok_SlashEqual) || peek(Tok_PercentEqual))
                    {
                        std::shared_ptr<FieldAssignment<Iter,Value,FuncType>> node;
                        if(accept(Tok_Equal))
                        {
                            node = std::make_shared<FieldAssignment<Iter,Value,FuncType>>();
                        }
                        else if(accept(Tok_PlusEqual))
                        {
                            node = std::make_shared<AddFieldAssignment<Iter,Value,FuncType>>();
                        }
                        else if(accept(Tok_DashEqual))
                        {
                            node = std::make_shared<SubFieldAssignment<Iter,Value,FuncType>>();
                        }
                        else if(accept(Tok_StarEqual))
                        {
                            node = std::make_shared<MulFieldAssignment<Iter,Value,FuncType>>();
                        }
                        else if(accept(Tok_SlashEqual))
                        {
                            node = std::make_shared<DivFieldAssignment<Iter,Value,FuncType>>();
                        }
                        else if(accept(Tok_PercentEqual))
                        {
                            node = std::make_shared<ModFieldAssignment<Iter,Value,FuncType>>();
                        }
                        node->begin = from;
                        node->children[0] = std::move(left);
                        node->children[1] = std::move(right);
                        node->children[2] = expression();
                        node->end = accepted+1;
                        left = std::move(node);
                    }
                    else
                    {
                        auto node = std::make_shared<FieldAccess<Iter,Value,FuncType>>();
                        node->begin = from;
                        node->children[0] = std::move(left);
                        node->children[1] = std::move(right);
                        node->end = accepted+1;
                        left = std::move(node);
                    }
                }
                else if(accept(Tok_Dot))
                {
                    auto right = field_name();
                    if(peek(Tok_Equal) || peek(Tok_PlusEqual) || peek(Tok_DashEqual) ||
                            peek(Tok_StarEqual) || peek(Tok_SlashEqual) || peek(Tok_PercentEqual))
                    {
                        std::shared_ptr<FieldAssignment<Iter,Value,FuncType>> node;
                        if(accept(Tok_Equal))
                        {
                            node = std::make_shared<FieldAssignment<Iter,Value,FuncType>>();
                        }
                        else if(accept(Tok_PlusEqual))
                        {
                            node = std::make_shared<AddFieldAssignment<Iter,Value,FuncType>>();
                        }
                        else if(accept(Tok_DashEqual))
                        {
                            node = std::make_shared<SubFieldAssignment<Iter,Value,FuncType>>();
                        }
                        else if(accept(Tok_StarEqual))
                        {
                            node = std::make_shared<MulFieldAssignment<Iter,Value,FuncType>>();
                        }
                        else if(accept(Tok_SlashEqual))
                        {
                            node = std::make_shared<DivFieldAssignment<Iter,Value,FuncType>>();
                        }
                        else if(accept(Tok_PercentEqual))
                        {
                            node = std::make_shared<ModFieldAssignment<Iter,Value,FuncType>>();
                        }
                        node->begin = from;
                        node->children[0] = std::move(left);
                        node->children[1] = std::move(right);
                        node->children[2] = expression();
                        node->end = accepted+1;
                        left = std::move(node);
                    }
                    else if(accept(Tok_LeftParen))
                    {
                        auto node = std::make_shared<MemberCall<Iter,Value,FuncType>>();
                        node->begin = from;
                        node->children.push_back(std::move(left));
                        node->children.push_back(std::move(right));
                        while(!accept(Tok_RightParen))
                        {
                            node->children.push_back(expression());
                            if(!peek(Tok_RightParen))
                            {
                                accept(Tok_Comma);
                            }
                        }
                        node->end = accepted+1;
                        left = std::move(node);
                    }
                    else
                    {
                        auto node = std::make_shared<FieldAccess<Iter,Value,FuncType>>();
                        node->begin = from;
                        node->children[0] = std::move(left);
                        node->children[1] = std::move(right);
                        node->end = accepted+1;
                        left = std::move(node);
                    }
                }
                else if(accept(Tok_LeftParen))
                {
                    auto node = std::make_shared<Call<Iter,Value,FuncType>>();
                    node->begin = from;
                    node->children.push_back(std::move(left));
                    while(!accept(Tok_RightParen))
                    {
                        node->children.push_back(expression());
                        if(!peek(Tok_RightParen))
                        {
                            accept(Tok_Comma);
                        }
                    }
                    node->end = accepted+1;
                    left = std::move(node);
                }
                else
                {
                    return std::move(left);
                }
            }
        }

        Parser::return_t Parser::unary_expression()
        {
            if(accept(Tok_Plus) || accept(Tok_Dash) || accept(Tok_Not))
            {
                using node_t = std::shared_ptr<UnaryExpression<Iter,Value,FuncType>>;
                node_t node;
                switch(accepted->type)
                {
                    case Tok_Plus:
                        node = node_t(new UnaryPlusExpression<Iter,Value,FuncType>);
                        break;
                    case Tok_Dash:
                        node = node_t(new UnaryMinusExpression<Iter,Value,FuncType>);
                        break;
                    case Tok_Not:
                        node = node_t(new UnaryNotExpression<Iter,Value,FuncType>);
                        break;
                    default:
                        break;
                }
                node->begin = accepted;
                node->children[0] = postfix_expression();
                node->end = accepted+1;
                return std::move(node);
            }
            else
            {
                return postfix_expression();
            }
        }

        Parser::return_t Parser::multiplicative_expression()
        {
            Iter from = i;
            auto left = unary_expression();
            if(accept(Tok_Star) || accept(Tok_Slash) || accept(Tok_Percent))
            {
                using node_t = std::shared_ptr<MultiplicativeExpression<Iter,Value,FuncType>>;
                node_t node;
                switch(accepted->type)
                {
                    case Tok_Star:
                        node = node_t(new MultiplyExpression<Iter,Value,FuncType>);
                        break;
                    case Tok_Slash:
                        node = node_t(new DivideExpression<Iter,Value,FuncType>);
                        break;
                    case Tok_Percent:
                        node = node_t(new ModuloExpression<Iter,Value,FuncType>);
                        break;
                    default:
                        break;
                }
                node->begin = from;
                node->children[0] = std::move(left);
                node->children[1] = multiplicative_expression();
                node->end = accepted+1;
                return std::move(node);
            }
            else
            {
                return std::move(left);
            }
        }

        Parser::return_t Parser::additive_expression()
        {
            Iter from = i;
            auto left = multiplicative_expression();
            if(accept(Tok_Plus) || accept(Tok_Dash))
            {
                using node_t = std::shared_ptr<AdditiveExpression<Iter,Value,FuncType>>;
                node_t node;
                switch(accepted->type)
                {
                    case Tok_Plus:
                        node = node_t(new AddExpression<Iter,Value,FuncType>);
                        break;
                    case Tok_Dash:
                        node = node_t(new SubtractExpression<Iter,Value,FuncType>);
                        break;
                    default:
                        break;
                }
                node->begin = from;
                node->children[0] = std::move(left);
                node->children[1] = additive_expression();
                node->end = accepted+1;
                return std::move(node);
            }
            else
            {
                return std::move(left);
            }
        }

        Parser::return_t Parser::relational_expression()
        {
            Iter from = i;
            auto left = additive_expression();
            if(accept(Tok_Less) || accept(Tok_LessEqual) || accept(Tok_Greater) || accept(Tok_GreaterEqual))
            {
                using node_t = std::shared_ptr<RelationalExpression<Iter,Value,FuncType>>;
                node_t node;
                switch(accepted->type)
                {
                    case Tok_Less:
                        node = node_t(new LessExpression<Iter,Value,FuncType>);
                        break;
                    case Tok_LessEqual:
                        node = node_t(new LessEqualExpression<Iter,Value,FuncType>);
                        break;
                    case Tok_Greater:
                        node = node_t(new GreaterExpression<Iter,Value,FuncType>);
                        break;
                    case Tok_GreaterEqual:
                        node = node_t(new GreaterEqualExpression<Iter,Value,FuncType>);
                        break;
                    default:
                        break;
                }
                node->begin = from;
                node->children[0] = std::move(left);
                node->children[1] = relational_expression();
                node->end = accepted+1;
                return std::move(node);
            }
            else
            {
                return std::move(left);
            }
        }

        Parser::return_t Parser::equality_expression()
        {
            Iter from = i;
            auto left = relational_expression();
            if(accept(Tok_EqOp) || accept(Tok_NotEqual))
            {
                using node_t = std::shared_ptr<EqualityExpression<Iter,Value,FuncType>>;
                node_t node;
                switch(accepted->type)
                {
                    case Tok_EqOp:
                        node = node_t(new EqualExpression<Iter,Value,FuncType>);
                        break;
                    case Tok_NotEqual:
                        node = node_t(new NotEqualExpression<Iter,Value,FuncType>);
                        break;
                    default:
                        break;
                }
                node->begin = from;
                node->children[0] = std::move(left);
                node->children[1] = relational_expression();
                node->end = accepted+1;
                return std::move(node);
            }
            else
            {
                return std::move(left);
            }
        }

        Parser::return_t Parser::logical_and_expression()
        {
            Iter from = i;
            auto left = equality_expression();
            if(accept(Tok_LogicalAnd))
            {
                auto node = std::make_shared<LogicalAndExpression<Iter,Value,FuncType>>();
                node->begin = from;
                node->children[0] = std::move(left);
                node->children[1] = equality_expression();
                node->end = accepted+1;
                return std::move(node);
            }
            else
            {
                return std::move(left);
            }
        }

        Parser::return_t Parser::logical_or_expression()
        {
            Iter from = i;
            auto left = logical_and_expression();
            if(accept(Tok_LogicalOr))
            {
                auto node = std::make_shared<LogicalOrExpression<Iter,Value,FuncType>>();
                node->begin = from;
                node->children[0] = std::move(left);
                node->children[1] = logical_and_expression();
                node->end = accepted+1;
                return std::move(node);
            }
            else
            {
                return std::move(left);
            }
        }

        Parser::return_t Parser::assignment_expression()
        {
            Iter from = i;
            if(peek(Tok_Global) || peek(Tok_Local))
            {
                bool global = accept(Tok_Global);
                bool local = accept(Tok_Local);
                auto left = field_name();
                if(accept(Tok_Equal))
                {
                    std::shared_ptr<AssignmentExpression<Iter,Value,FuncType>> node;
                    node =
                        global ?
                            std::make_shared<GlobalAssignmentExpression<Iter,Value,FuncType>>()
                        :
                            local ?
                            std::make_shared<LocalAssignmentExpression<Iter,Value,FuncType>>()
                            :
                                std::make_shared<AssignmentExpression<Iter,Value,FuncType>>();
                    node->begin = from;
                    node->children[0] = std::move(left);
                    node->children[1] = assignment_expression();
                    node->end = accepted+1;
                    return std::move(node);
                }
            }
            else if(peek(Tok_Identifier))
            {
                auto left = field_name();
                std::shared_ptr<AssignmentExpression<Iter,Value,FuncType>> node;
                if(accept(Tok_Equal))
                {
                    node = std::make_shared<AssignmentExpression<Iter,Value,FuncType>>();
                }
                else if(accept(Tok_PlusEqual))
                {
                    node = std::make_shared<AddAssignmentExpression<Iter,Value,FuncType>>();
                }
                else if(accept(Tok_DashEqual))
                {
                    node = std::make_shared<SubAssignmentExpression<Iter,Value,FuncType>>();
                }
                else if(accept(Tok_StarEqual))
                {
                    node = std::make_shared<MulAssignmentExpression<Iter,Value,FuncType>>();
                }
                else if(accept(Tok_SlashEqual))
                {
                    node = std::make_shared<DivAssignmentExpression<Iter,Value,FuncType>>();
                }
                else if(accept(Tok_PercentEqual))
                {
                    node = std::make_shared<ModAssignmentExpression<Iter,Value,FuncType>>();
                }
                else
                {
                    rewind(from);
                    return logical_or_expression();
                }
                node->begin = from;
                node->children[0] = std::move(left);
                node->children[1] = assignment_expression();
                node->end = accepted+1;
                return std::move(node);
            }
            rewind(from);
            return logical_or_expression();
        }

        Parser::return_t Parser::initializer_assignment_expression()
        {
            auto node = std::make_shared<InitializerAssignmentExpression<Iter,Value,FuncType>>();
            node->begin = i;
            node->children.push_back(logical_or_expression());
            if(accept(Tok_Equal))
            {
                node->children.push_back(logical_or_expression());
            }
            node->end = accepted+1;
            return std::move(node);
        }

        Parser::return_t Parser::expression()
        {
            return assignment_expression();
        }

        Parser::return_t Parser::block()
        {
            auto node = std::make_shared<Block<Iter,Value,FuncType>>();
            node->begin = i;
            expect(Tok_LeftBrace);
            node->children[0] = statement_list();
            expect(Tok_RightBrace);
            node->end = accepted+1;
            return std::move(node);
        }

        Parser::return_t Parser::if_statement()
        {
            auto node = std::make_shared<IfStatement<Iter,Value,FuncType>>();
            node->begin = i;
            expect(Tok_If);
            expect(Tok_LeftParen);
            node->children.push_back(expression());
            expect(Tok_RightParen);
            node->children.push_back(statement());
            if(accept(Tok_Else))
            {
                node->children.push_back(statement());
            }
            node->end = accepted+1;
            return std::move(node);
        }

        Parser::return_t Parser::while_statement()
        {
            auto node = std::make_shared<WhileStatement<Iter,Value,FuncType>>();
            node->begin = i;
            expect(Tok_While);
            expect(Tok_LeftParen);
            node->children[0] = expression();
            expect(Tok_RightParen);
            node->children[1] = statement();
            node->end = accepted+1;
            return std::move(node);
        }

        Parser::return_t Parser::nop()
        {
            auto node = std::make_shared<Nop<Iter,Value,FuncType>>();
            node->begin = i;
            node->end = i;
            return std::move(node);
        }

        Parser::return_t Parser::for_statement()
        {
            Iter from = i;
            expect(Tok_For);
            expect(Tok_LeftParen);
            if(peek(Tok_Identifier) && (peek(Tok_Comma, 1) || peek(Tok_Colon, 1)))
            {
                auto node = std::make_shared<ForEachStatement<Iter,Value,FuncType>>();
                node->begin = from;
                node->children.push_back(field_name());
                if(accept(Tok_Comma))
                {
                    node->children.push_back(field_name());
                }
                expect(Tok_Colon);
                node->children.push_back(expression());
                expect(Tok_RightParen);
                node->children.push_back(statement());
                node->end = accepted+1;
                return std::move(node);
            }
            else
            {
                auto node = std::make_shared<ForStatement<Iter,Value,FuncType>>();
                node->begin = from;
                if(accept(Tok_Semicolon))
                {
                    node->children.push_back(nop());
                }
                else
                {
                    node->children.push_back(expression());
                    expect(Tok_Semicolon);
                }
                if(accept(Tok_Semicolon))
                {
                    node->children.push_back(nop());
                }
                else
                {
                    node->children.push_back(expression());
                    expect(Tok_Semicolon);
                }
                if(accept(Tok_RightParen))
                {
                    node->children.push_back(nop());
                }
                else
                {
                    node->children.push_back(expression());
                    expect(Tok_RightParen);
                }
                node->children.push_back(statement());
                node->end = accepted+1;
                return std::move(node);
            }
        }

        Parser::return_t Parser::break_statement()
        {
            auto node = std::make_shared<BreakStatement<Iter,Value,FuncType>>();
            node->begin = i;
            expect(Tok_Break);
            accept(Tok_Semicolon);
            node->end = accepted+1;
            return std::move(node);
        }

        Parser::return_t Parser::continue_statement()
        {
            auto node = std::make_shared<ContinueStatement<Iter,Value,FuncType>>();
            node->begin = i;
            expect(Tok_Continue);
            accept(Tok_Semicolon);
            node->end = accepted+1;
            return std::move(node);
        }

        Parser::return_t Parser::return_statement()
        {
            auto node = std::make_shared<ReturnStatement<Iter,Value,FuncType>>();
            node->begin = i;
            expect(Tok_Return);
            if(accept(Tok_Semicolon))
            {
                node->end = accepted+1;
                return std::move(node);
            }
            node->children.push_back(expression());
            accept(Tok_Semicolon);
            node->end = accepted+1;
            return std::move(node);
        }

        Parser::return_t Parser::statement()
        {
            if(peek(Tok_LeftBrace))
            {
                return block();
            }
            else if (peek(Tok_If))
            {
                return if_statement();
            }
            else if (peek(Tok_While))
            {
                return while_statement();
            }
            else if (peek(Tok_For))
            {
                return for_statement();
            }
            else if (peek(Tok_Return))
            {
                return return_statement();
            }
            else if (peek(Tok_Continue))
            {
                return continue_statement();
            }
            else if (peek(Tok_Break))
            {
                return break_statement();
            }
            else
            {
                auto node = std::make_shared<ExpressionStatement<Iter,Value,FuncType>>();
                node->begin = i;
                node->children[0] = expression();
                accept(Tok_Semicolon);
                node->end = accepted+1;
                return std::move(node);
            }
        }

        Parser::return_t Parser::statement_list()
        {
            auto node = std::make_shared<StatementList<Iter,Value,FuncType>>();
            node->begin = i;
            while(!end_of_input() && !peek(Tok_RightBrace))
            {
                node->children.push_back(statement());
            }
            node->end = accepted+1;
            return std::move(node);
        }
    }
}
