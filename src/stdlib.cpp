
#include "private.h"
#include "builtin.h"

namespace Aqua
{
    namespace Stdlib
    {
        void fn_print(Interpreter&, std::vector<Interpreter::Value> &stack)
        {
            for(size_t i = 1; i<stack.size(); ++i)
            {
                std::cout << stack[i];
            }
            stack.clear();
        }

        void fn_println(Interpreter& ip, std::vector<Interpreter::Value> &stack)
        {
            fn_print(ip, stack);
            std::cout << std::endl;
            stack.clear();
        }

        void fn_readline(Interpreter&, std::vector<Interpreter::Value> &stack)
        {
            stack.clear();
            std::string str;
            std::getline(std::cin, str);
            stack.push_back(Interpreter::Value(str));
        }

        void fn_children(Interpreter&, std::vector<Interpreter::Value> &stack)
        {
            using Value = Interpreter::Value;
            if(stack.empty())
            {
                return;
            }
            auto arg = stack[1];
            Value::Type t = arg.type();
            stack.clear();
            if(t == Value::Type::Tree || t == Value::Type::Function)
            {
                auto result = std::make_shared<Array<Interpreter::Value>>();
                for(size_t i = 0, s = arg.pointer()->size(); i<s; ++i)
                {
                    result->add(Value(Value::Type::Tree, (*arg.pointer())[i]));
                }
                stack.push_back(result);
            }
        }

        void fn_tokens(Interpreter&, std::vector<Interpreter::Value> &stack)
        {
            using Value = Interpreter::Value;
            if(stack.empty())
            {
                return;
            }
            auto arg = stack[1];
            Value::Type t = arg.type();
            stack.clear();
            if(t == Value::Type::Tree || t == Value::Type::Function)
            {
                auto result = std::make_shared<Array<Interpreter::Value>>();
                auto node = arg.pointer();
                for(auto i = node->begin; i!=node->end; ++i)
                {
                    result->add(Value(std::string(i->begin, i->end)));
                }
                stack.push_back(result);
            }
        }

        void fn_expand_node(Interpreter&, std::vector<Interpreter::Value> &stack)
        {
            using Value = Interpreter::Value;
            if(stack.empty())
            {
                return;
            }
            auto arg = stack[1];
            Value::Type t = arg.type();
            stack.clear();
            if(t == Value::Type::Tree || t == Value::Type::Function)
            {
                auto result = std::make_shared<Array<Interpreter::Value>>();
                auto &node = *arg.pointer();
                auto token = node.begin;
                for(size_t i = 0, s = arg.pointer()->size(); i<s; ++i)
                {
                    for(; token!=node[i]->begin; ++token)
                    {
                        result->add(Value(std::string(token->begin, token->end)));
                    }
                    result->add(Value(Value::Type::Tree, node[i]));
                    token = node[i]->end;
                }
                for(; token!=node.end; ++token)
                {
                    result->add(Value(std::string(token->begin, token->end)));
                }
                stack.push_back(result);
            }
        }

        void fn_tostring(Interpreter&, std::vector<Interpreter::Value> &stack)
        {
            using Value = Interpreter::Value;
            if(stack.empty())
            {
                return;
            }
            auto arg = stack[1];
            stack.clear();
            switch(arg.type())
            {
                case Value::Type::Tree:
                    stack.emplace_back(std::string(arg.tree()->begin->begin, (arg.tree()->end-1)->end));
                    return;
                case Value::Type::Function:
                    stack.emplace_back(std::string(arg.function()->begin->begin, (arg.function()->end-1)->end));
                    return;
                case Value::Type::Number:
                    stack.emplace_back(std::to_string(arg.number()));
                    return;
                case Value::Type::String:
                    stack.push_back(arg);
                    return;
                default:
                    stack.clear();
                    return;
            }
        }

        void fn_tonumber(Interpreter&, std::vector<Interpreter::Value> &stack)
        {
            using Value = Interpreter::Value;
            if(stack.empty())
            {
                return;
            }
            auto arg = stack[1];
            stack.clear();
            if(arg.type() == Value::Type::String)
            {
                Value::NumberType result = std::strtod(arg.string().c_str(), nullptr);
                if(result != HUGE_VAL)
                {
                    stack.emplace_back(result);
                }
            }
        }
    }
}
