
#include "private.h"
#include "parser.h"

namespace Aqua
{
    using namespace Internal;

    Interpreter::Value Interpreter::operator()(ExpressionStatement<Iter,Interpreter::Value,FuncType> &node)
    {
        return node.children[0]->accept(*this);
    }

    Interpreter::Value Interpreter::operator()(ReturnStatement<Iter,Interpreter::Value,FuncType> &node)
    {
        if(node.children.size()>0)
        {
            m_function_stack.push_back(node.children[0]->accept(*this));
        }
        return Interpreter::Value(typename Interpreter::Value::TagReturn());
    }

    /*
    Interpreter::Value Interpreter::operator()(EvalStatement<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value treeval = node.children[0]->accept(*this);
        if(treeval.type() != Interpreter::Value::Type::Tree)
        {
            error("cannot eval non-tree.", node);
            return Interpreter::Value();
        }
        return treeval.tree()->accept(*this);
    }
    */

    Interpreter::Value Interpreter::operator()(Eval<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value treeval = node.children[0]->accept(*this);
        if(treeval.type() != Interpreter::Value::Type::Tree)
        {
            error("cannot eval non-tree.", node);
            return Interpreter::Value();
        }
        return treeval.tree()->accept(*this);
    }


    Interpreter::Value Interpreter::operator()(ParseStatement<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value val = node.children[0]->accept(*this);
        if(val.type() != Interpreter::Value::Type::String)
        {
            error("parse argument needs to be string.", node);
            return Interpreter::Value();
        }
        return Interpreter::Value(Interpreter::Value::Type::Tree, (*m_parser)(val.string().begin(), val.string().end()));
    }

    Interpreter::Value Interpreter::operator()(ParseExpression<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value val = node.children[0]->accept(*this);
        if(val.type() != Interpreter::Value::Type::String)
        {
            error("parse argument needs to be string.", node);
            return Interpreter::Value();
        }
        return Interpreter::Value(Interpreter::Value::Type::Tree, (*m_parser).parse_expression(
            val.string().begin(), val.string().end()));
    }


    Interpreter::Value Interpreter::operator()(StatementList<Iter,Interpreter::Value,FuncType> &node)
    {
        for(auto child : node.children)
        {
            Interpreter::Value result = child->accept(*this);
            typename Interpreter::Value::Type t = result.internal_type();
            if(t >= Interpreter::Value::Type::Return && t<= Interpreter::Value::Type::Continue)
            {
                return result;
            }
        }
        return Interpreter::Value();
    }

    Interpreter::Value Interpreter::operator()(IfStatement<Iter,Interpreter::Value,FuncType> &node)
    {
        Interpreter::Value cond = node.children[0]->accept(*this);
        if(cond.type() != Interpreter::Value::Type::Number)
        {
            error("if condition not a number.", node);
            return Interpreter::Value();
        }
        if(cond.number() != 0.0)
        {
            return node.children[1]->accept(*this);
        }
        else if(node.children.size()>2)
        {
            return node.children[2]->accept(*this);
        }
        return Interpreter::Value();
    }

    Interpreter::Value Interpreter::operator()(WhileStatement<Iter,Interpreter::Value,FuncType> &node)
    {
        auto nullval = Value(0.0);
        while(true)
        {
            auto cond = node.children[0]->accept(*this);
            if(cond.type() != Interpreter::Value::Type::Number)
            {
                error("while condition not a number.", node);
            }
            if(cond == nullval)
            {
                break;
            }
            auto result = node.children[1]->accept(*this);
            typename Interpreter::Value::Type t = result.internal_type();
            if(t == Interpreter::Value::Type::Break || t == Interpreter::Value::Type::Return)
            {
                return result;
            }
        }
        return Interpreter::Value();
    }

    Interpreter::Value Interpreter::operator()(ForStatement<Iter,Interpreter::Value,FuncType> &node)
    {
        LocalScope(*this);
        auto nullval = Value(0.0);
        node.children[0]->accept(*this);
        while(true)
        {
            Interpreter::Value cond = node.children[1]->accept(*this);
            if(cond.type() != Interpreter::Value::Type::Number)
            {
                error("while condition not a number.", node);
            }
            if(cond == nullval)
            {
                break;
            }
            Interpreter::Value result = node.children[3]->accept(*this);
            typename Interpreter::Value::Type t = result.internal_type();
            if(t == Interpreter::Value::Type::Break || t == Interpreter::Value::Type::Return)
            {
                return result;
            }
            node.children[2]->accept(*this);
        }
        return Interpreter::Value();
    }

    Interpreter::Value Interpreter::operator()(ForEachStatement<Iter,Interpreter::Value,FuncType> &node)
    {
        if(node.children.size()==3)
        {
            Interpreter::Value value = node.children[0]->accept(*this);
            Interpreter::Value table = node.children[1]->accept(*this);
            Interpreter::Value::Type t = table.type();
            if(t != Interpreter::Value::Type::Table && t != Interpreter::Value::Type::Array)
            {
                error("for each argument is not a table or array", node);
            }
            LocalScope local(*this);
            if(t == Interpreter::Value::Type::Table)
            {
                for(auto entry : *table.table())
                {
                    setLocal(value, entry.second);
                    Interpreter::Value result = node.children[2]->accept(*this);
                    typename Interpreter::Value::Type result_t = result.internal_type();
                    if(result_t == Interpreter::Value::Type::Break || result_t == Interpreter::Value::Type::Return)
                    {
                        return result;
                    }
                }
            }
            else
            {
                for(auto entry : *table.array())
                {
                    setLocal(value, entry);
                    Interpreter::Value result = node.children[2]->accept(*this);
                    typename Interpreter::Value::Type result_t = result.type();
                    if(result_t == Interpreter::Value::Type::Break || result_t == Interpreter::Value::Type::Return)
                    {
                        return result;
                    }
                }
            }
        }
        else
        {
            Interpreter::Value key = node.children[0]->accept(*this);
            Interpreter::Value value = node.children[1]->accept(*this);
            Interpreter::Value table = node.children[2]->accept(*this);
            if(table.type() != Interpreter::Value::Type::Table)
            {
                error("for each argument is not a table", node);
            }
            LocalScope local(*this);
            for(auto entry : *table.table())
            {
                setLocal(key, entry.first);
                setLocal(value, entry.second);
                Interpreter::Value result = node.children[3]->accept(*this);
                typename Interpreter::Value::Type t = result.type();
                if(t == Interpreter::Value::Type::Break || t == Interpreter::Value::Type::Return)
                {
                    return result;
                }
            }
        }
        return Interpreter::Value();
    }

    Interpreter::Value Interpreter::operator()(ContinueStatement<Iter,Interpreter::Value,FuncType> &)
    {
        return Interpreter::Value(typename Interpreter::Value::TagContinue());
    }

    Interpreter::Value Interpreter::operator()(BreakStatement<Iter,Interpreter::Value,FuncType> &)
    {
        return Interpreter::Value(typename Interpreter::Value::TagBreak());
    }
}

