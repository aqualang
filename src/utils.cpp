
#include "private.h"

namespace Aqua
{
    namespace Utils
    {
        std::string Unescape(const std::string &str)
        {
            std::string result;
            for(auto i = str.begin(); i!=str.end(); i++)
            {
                if(*i == '\\')
                {
                    switch(*(++i))
                    {
                        case 'a':
                            result += '\a';
                            break;
                        case 'b':
                            result += '\b';
                            break;
                        case 'f':
                            result += '\f';
                            break;
                        case 'n':
                            result += '\n';
                            break;
                        case 'r':
                            result += '\r';
                            break;
                        case 't':
                            result += '\t';
                            break;
                        case 'v':
                            result += '\v';
                            break;
                        case '\'':
                            result += '\'';
                            break;
                        case '\"':
                            result += '\"';
                            break;
                    }
                }
                else
                {
                    result += *i;
                }
            }
            return result;
        }
    }
}

