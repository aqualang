
#include "private.h"
#include "builtin.h"
#include "parser.h"

namespace Aqua
{
    using namespace Builtins;
    using Value = Interpreter::Value;

    static void populate_modpath(Interpreter& ip)
    {
        ip.addModulePath(".");
        ip.addModulePath("./modules");
    }

    template<typename Stream>
    static Value make_standard_handle(Interpreter* ip, Stream& strm)
    {
        auto tb = ip->makeTable();
        tb->set(ID_HANDLE, ip->makeUserPointer(&strm));
        for(auto pair: Builtins::FileMembers)
        {
            tb->set(pair.first, ip->makeFunction(pair.second));
        }
        return tb;
    }

    Value Interpreter::makeString(const std::string& src)
    {
        return Value(src);
    }

    Value Interpreter::makeNumber(Value::NumberType val)
    {
        return Value(Value::NumberType(val));
    }

    std::shared_ptr<Array<Value>> Interpreter::makeArray()
    {
        return std::make_shared<Array<Value>>();
    }

    std::shared_ptr<Table<Value>> Interpreter::makeTable()
    {
        return std::make_shared<Table<Value>>();
    }

    Value Interpreter::makeFunction(Interpreter::FuncProto fun)
    {
        auto node = std::make_shared<Internal::Function<Iter,Value,FuncType>>();
        node->begin = node->end;
        auto nop = std::make_shared<Internal::Nop<Iter,Value,FuncType>>();
        nop->begin = nop->end;
        node->children[0] = nop;
        auto builtin = std::make_shared<Internal::BuiltinFunction<Iter,Value,FuncType>>();
        builtin->begin = builtin->end;
        builtin->function = fun;
        node->children[1] = builtin;
        return Value(Value::Type::Function, node);
    }

    bool Interpreter::need(const Value& raw_in, Value& dest, Value::Type expected)
    {
        auto intype = raw_in.type();
        if(intype == expected)
        {
            dest = raw_in;
            return true;
        }
        return false;
    }

    bool Interpreter::needNumber(const Value& raw_in, Value& dest)
    {
        return need(raw_in, dest, Value::Type::Number);
    }

    bool Interpreter::needString(const Value& raw_in, Value& dest)
    {
        return need(raw_in, dest, Value::Type::String);
    }

    bool Interpreter::needFunction(const Value& raw_in, Value& dest)
    {
        return need(raw_in, dest, Value::Type::Function);
    }

    void Interpreter::beginLocalScope()
    {
        m_envstack.push_back(std::make_shared<Table<Value>>());
    }

    void Interpreter::endLocalScope()
    {
        m_envstack.pop_back();
    }

    void Interpreter::beginFunctionScope()
    {
        m_funstack.push_back(m_envstack.size());
        m_envstack.push_back(std::make_shared<Table<Value>>());
    }

    void Interpreter::endFunctionScope()
    {
        m_envstack.pop_back();
        m_funstack.pop_back();
    }

    Value Interpreter::get(Value key)
    {
        int top = m_envstack.size()-1;
        int bottom = m_funstack.back();
        for(int i = top; i>=bottom; --i)
        {
            Value value = m_envstack[i]->get(key);
            if(value.type() != Value::Type::Nil)
            {
                return value;
            }
        }
        return m_envstack[0]->get(key);
    }

    void Interpreter::set(Value key, Value value)
    {
        m_envstack[m_funstack.back()]->set(key, value);
    }

    void Interpreter::setLocal(Value key, Value value)
    {
        m_envstack.back()->set(key, value);
    }

    void Interpreter::setGlobal(Value key, Value value)
    {
        m_envstack[0]->set(key, value);
    }

    void Interpreter::setGlobalFunc(const std::string &name, Interpreter::FuncProto fun)
    {
        setGlobal(Value(name), makeFunction(fun));
    }

    Interpreter::ModulePath Interpreter::getModulePath()
    {
        return m_modpath;
    }

    void Interpreter::addModulePath(const std::string& path)
    {
        m_modpath.push_back(path);
    }

    Interpreter::Interpreter()
    {
        m_parser = new Internal::Parser;
        beginFunctionScope();
        populate_modpath(*this);
        setGlobal(Value("true"),  Value(Value::NumberType(1)));
        setGlobal(Value("false"), Value(Value::NumberType(0)));
        setGlobalFunc("print", Stdlib::fn_print);
        setGlobalFunc("println", Stdlib::fn_println);
        setGlobalFunc("readline", Stdlib::fn_readline);
        setGlobalFunc("children", Stdlib::fn_children);
        setGlobalFunc("tokens", Stdlib::fn_tokens);
        setGlobalFunc("expand_node", Stdlib::fn_expand_node);
        setGlobalFunc("tostring", Stdlib::fn_tostring);
        setGlobalFunc("tonumber", Stdlib::fn_tonumber);
        // this creates the "File" class
        setGlobalFunc("File", OBJMEMBER_NAME(File, constructor));

        // setting up IO table
        auto iotb = makeTable();
        iotb->set(makeString("stdout"), make_standard_handle(this, std::cout));
        iotb->set(makeString("stderr"), make_standard_handle(this, std::cerr));
        iotb->set(makeString("stdin"), make_standard_handle(this, std::cin));
        setGlobal(makeString("IO"), iotb);
    }

    Interpreter::~Interpreter()
    {
        endFunctionScope();
        delete m_parser;
    }

    Value Interpreter::eval(const std::string &source)
    {
        (void)source;
        //LocalScope scope(*this);
    #if 1
        //auto psr = m_parser(source.begin(), source.end());
        //auto root = m_parser.eval()->children[0];
        //return (*this)(*root);
        //return root->accept(*this);
        //return result;
        //return root->tree()->accept(*this);
        //return_t parse_expression(StrIter sbegin, StrIter send)
        //auto node = std::make_shared<Internal::Eval<Iter,Value,FuncType>>();
    #else
        ParserInstance ps;
        ps(source.begin(), source.end());
        Value result = ps.eval()->accept(*this);
        return result;
    #endif
        return Value();

    }


    Value Interpreter::run(const std::string &source)
    {
        auto root = (*m_parser)(source.begin(), source.end());
        Value result = root->accept(*this);
        return result;
    }
}
