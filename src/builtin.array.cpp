

#include "private.h"
#include "builtin.h"

namespace Aqua
{
    using Value = Interpreter::Value;

    namespace Builtins
    {
        BUILTIN_PROTO(rtbuiltin_func_array_length, ip, self, args)
        {
            (void)ip;
            (void)args;
            Value::NumberType sz;
            sz = Value::NumberType(self.array()->size());
            return Value(sz);
        }

        BUILTIN_PROTO(rtbuiltin_func_array_slice, ip, self, args)
        {
            (void)ip;
            (void)self;
            (void)args;
            return Value();
        }
    }
}
