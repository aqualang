
#include <map>
#include "aqua.h"

#define ID_HANDLE Interpreter::Value(Interpreter::Value::NumberType('h'))

template<class T>
bool isint(T x)
{
    return x == std::trunc(x);
}

namespace Aqua
{
    //! todo: create a wrapper method ...
    namespace Stdlib
    {
        void fn_print(Interpreter&, std::vector<Interpreter::Value> &stack);
        void fn_println(Interpreter&, std::vector<Interpreter::Value> &stack);
        void fn_readline(Interpreter&, std::vector<Interpreter::Value> &stack);
        void fn_children(Interpreter&, std::vector<Interpreter::Value> &stack);
        void fn_tokens(Interpreter&, std::vector<Interpreter::Value> &stack);
        void fn_expand_node(Interpreter&, std::vector<Interpreter::Value> &stack);
        void fn_tostring(Interpreter&, std::vector<Interpreter::Value> &stack);
        void fn_tonumber(Interpreter&, std::vector<Interpreter::Value> &stack);
    }
}
