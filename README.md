# Aqua

A ~~toy~~ programming language interpreter

## building

~~the jtc interpreter has no dependencies and sits in a single C++ file.~~
~~C++11 capable compilers should compile it without extra options other~~
~~than enabling C++11.~~

~~example: `g++ -o jtc jtc.cpp -std=c++0x -O3`~~

Not anymore! Sorry :-( ... To build jtc, you'll need cmake now.<br />
Quick setup:

    mkdir build
    cd build
    cmake ..
    make # be patient!
    # all set

The main executable is `aqua`.<br />
The library is 'libaqua.so' (or `aqua.dll` on windows).

The entire API (which is still awaiting some rewrital) is in include/aqua.h.<br/>
For now, `src/main.cpp`, as well as `src/stdlib*.cpp` need to suffice as API examples ... though I'm working on better examples.

## syntax

**keywords:**

 + **`if`**

  Does what it says on the tin.<br />
  Example:

        if(1 == 2)
        {
            println("nuh uh!")
        }

 + **`while`**

    Looping.<br />
    Example:

        while(1 < 2)
        {
            println("waiting for the impossible. please stand by")
        }

 + **``**

 + **`sizeof`**

  Gets the size of an object using `<Container>::size()` (i.e, `Array::size`).<br />
  Example:

        things = ["foo", "bar", "baz"]
        println(sizeof(things))
        //same as `println(things.size())`

 + **`typeof`**

  Gets the type of an object using `Value::typeName()`.<br />
  Example:

        MyClass = func()
        {
            local this = {}
            ...
            return this
        }

        println(typeof(MyClass))


**Builtin functions and Type member functions**

 **Builtins for strings**

  todo: still not completely done

**Variables defined by the VM, and implicitly set variables**:

 + **`true`**

  Evaluates to `1`. Self-explanatory.

 + **`false`**

  Evaluates to `0`. Self-explanatory.

 + **`ARGV`**

  Contains the arguments passed to a script or `-e`. Only set by the main interpreter;
  You'll have to define it yourself if you embed aqua.

 + **`$args`**

  Contains all arguments passed to a function. Only defined within a function scope.<br />
  Example:

        variadic = func()
        {
            for(arg: $args)
            {
                println("arg = ", arg)
            }
        }

        func("foo", [1, 2, 3], "hurf", func(){}, "that's all, folks!")

## todo

1. ~~Better name? `jtc` is more of a temporary name. Maybe this whole thing grows into something more~~ Update: renamed to `aqua`!

2. JIT-/Bytecode-support is pretty much WONTFIX, but it's nice to think about.

3. ~~Rewrite of the API. Many parts are as good as it gets (though tend to be arguably awkward -- see `src/ops.cpp`), but there's a slight template-induced codesmell.~~<br />
~~Having a better API might also reduce compile times, and make it easier to optimize.~~
Let's reword that: There's need to bring in a lil' order in some of the messier places. There are still a handful of
global functions that *might* prove problematic. *Might*.

4. Wrap the whole mess in a namespace. Pretty much requires point #1.

5. Better runtime library. Not really a priority for now, though. Starting points can be found in `src/{private.h,stdlib*.cpp}`. Update: Currently in progress.
