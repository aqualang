
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <array>
#include <functional>
#include <unordered_map>
#include <stdexcept>
#include <memory>
#include <cctype>
#include <cmath>

namespace Aqua
{
    namespace Utils
    {
        std::string Unescape(const std::string &str);

        // helper to find line & column position on demand
        template<class Iter>
        std::pair<int, int> LineColumn(Iter begin, Iter pos)
        {
            int line = 1;
            int column = 0;
            for(Iter i = begin; i!=pos; ++i)
            {
                if(*i == '\n')
                {
                    ++line;
                    column = 0;
                }
                ++column;
            }
            return std::make_pair(line, column);
        }
    }

    class Interpreter;

    namespace Internal
    {
        enum TokenType
        {
            Tok_Number,          Tok_Identifier,        Tok_String,
            Tok_LeftParen,       Tok_RightParen,        Tok_LeftBracket,
            Tok_RightBracket,    Tok_LeftBrace,         Tok_RightBrace,
            Tok_Plus,            Tok_Dash,              Tok_Star,
            Tok_Percent,         Tok_Slash,             Tok_PlusEqual,
            Tok_DashEqual,       Tok_StarEqual,         Tok_PercentEqual,
            Tok_SlashEqual,      Tok_Not,               Tok_Comma,
            Tok_Semicolon,       Tok_Dot,               Tok_Colon,
            Tok_Equal,           Tok_EqOp,              Tok_NotEqual,
            Tok_Greater,         Tok_GreaterEqual,      Tok_Less,
            Tok_LessEqual,       Tok_LogicalAnd,        Tok_LogicalOr,
            Tok_If,              Tok_Else,              Tok_While,
            Tok_For,             Tok_Return,            Tok_Break,
            Tok_Continue,        Tok_Function,          Tok_Expression,
            Tok_Statement,       Tok_Root,              Tok_Nil,
            Tok_Eval,            Tok_ParseStatement,    Tok_ParseExpression,
            Tok_Global,          Tok_Local,             Tok_Print,
            Tok_TypeOf,          Tok_SizeOf,            Tok_Switch,
            Tok_Case,            Tok_Import,
        };

        template<class Iter>
        struct Token
        {
            const TokenType type;
            const Iter begin;
            const Iter end;

            Token(TokenType type_, Iter begin_, Iter end_):
                type(type_),
                begin(begin_),
                end(end_)
            {
            }
        };

        // Abstract Syntax Tree node types
        template<typename Iter, typename Value, typename FuncType>
        struct ASTNode: public std::enable_shared_from_this<ASTNode<Iter,Value,FuncType>>
        {
            Iter begin;
            Iter end;
            std::shared_ptr<std::string> source;
            std::shared_ptr<std::vector<Token<std::string::iterator>>> tokens;
            std::weak_ptr<ASTNode<Iter,Value,FuncType>> root;

            virtual Value accept(FuncType&) = 0;

            virtual std::shared_ptr<ASTNode<Iter,Value,FuncType>>& operator[](size_t) = 0;

            virtual size_t size() const = 0;

            virtual void inject_dependencies() = 0;
        };

        template<class Iter, class Value, class FuncType>
        struct Variadic : public ASTNode<Iter,Value,FuncType>
        {
            std::vector<std::shared_ptr<ASTNode<Iter,Value,FuncType>>> children;

            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }

            virtual std::shared_ptr<ASTNode<Iter,Value,FuncType>>& operator[](size_t i)
            {
                return children[i];
            }

            virtual size_t size() const
            {
                return children.size();
            }

            virtual void inject_dependencies()
            {
                for(auto& child : children)
                {
                    child->root = this->root;
                    child->source = this->source;
                    child->tokens = this->tokens;
                    child->inject_dependencies();
                }
            }
        };

        template<class Iter, class Value, class FuncType>
        struct Ternary : public ASTNode<Iter,Value,FuncType>
        {
            std::array<std::shared_ptr<ASTNode<Iter,Value,FuncType>>, 3> children;

            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }

            virtual std::shared_ptr<ASTNode<Iter,Value,FuncType>>& operator[](size_t i)
            {
                return children[i];
            }

            virtual size_t size() const
            {
                return children.size();
            }

            virtual void inject_dependencies()
            {
                for(auto& child : children)
                {
                    child->root = this->root;
                    child->source = this->source;
                    child->tokens = this->tokens;
                    child->inject_dependencies();
                }
            }
        };

        template<class Iter, class Value, class FuncType>
        struct Binary : public ASTNode<Iter,Value,FuncType>
        {
            std::array<std::shared_ptr<ASTNode<Iter,Value,FuncType>>, 2> children;

            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }

            virtual std::shared_ptr<ASTNode<Iter,Value,FuncType>>& operator[](size_t i)
            {
                return children[i];
            }

            virtual size_t size() const
            {
                return children.size();
            }

            virtual void inject_dependencies()
            {
                for(auto& child : children)
                {
                    child->root = this->root;
                    child->source = this->source;
                    child->tokens = this->tokens;
                    child->inject_dependencies();
                }
            }
        };

        template<class Iter, class Value, class FuncType>
        struct Unary : public ASTNode<Iter,Value,FuncType>
        {
            std::array<std::shared_ptr<ASTNode<Iter,Value,FuncType>>, 1> children;

            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }

            virtual std::shared_ptr<ASTNode<Iter,Value,FuncType>>& operator[](size_t i)
            {
                return children[i];
            }

            virtual size_t size() const
            {
                return children.size();
            }

            virtual void inject_dependencies()
            {
                for(auto& child : children)
                {
                    child->root = this->root;
                    child->source = this->source;
                    child->tokens = this->tokens;
                    child->inject_dependencies();
                }
            }
        };

        template<class Iter, class Value, class FuncType>
        struct Nullary : public ASTNode<Iter,Value,FuncType>
        {
            std::array<std::shared_ptr<ASTNode<Iter,Value,FuncType>>, 0> children;

            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }

            virtual std::shared_ptr<ASTNode<Iter,Value,FuncType>>& operator[](size_t i)
            {
                return children[i];
            }

            virtual size_t size() const
            {
                return children.size();
            }

            virtual void inject_dependencies()
            {
                for(auto& child : children)
                {
                    child->root = this->root;
                    child->source = this->source;
                    child->tokens = this->tokens;
                    child->inject_dependencies();
                }
            }
        };

        template<class Iter, class Value, class FuncType>
        struct Variable : public Nullary<Iter,Value,FuncType>
        {
            Value name;

            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct GlobalVariable : public Variable<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct LocalVariable : public Variable<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct FieldName : public Nullary<Iter,Value,FuncType>
        {
            Value name;
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct Nop : public Nullary<Iter,Value,FuncType>
        {
            double value;
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct Constant : public Nullary<Iter,Value,FuncType>
        {
            Value value;

            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct Nil : public Nullary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct String : public Nullary<Iter,Value,FuncType>
        {
            Value value;

            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct FieldAccess : public Binary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct FieldAssignment : public Ternary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct AddFieldAssignment : public FieldAssignment<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct SubFieldAssignment : public FieldAssignment<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct MulFieldAssignment : public FieldAssignment<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct DivFieldAssignment : public FieldAssignment<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct ModFieldAssignment : public FieldAssignment<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct Call : public Variadic<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct MemberCall : public Variadic<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        /*
        template<class Iter, class Value, class FuncType>
        struct EvalStatement : public Unary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };
        */

        template<class Iter, class Value, class FuncType>
        struct Eval : public Unary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct ParseStatement : public Unary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct ParseExpression : public Unary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct UnaryExpression : public Unary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct UnaryPlusExpression : public UnaryExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct UnaryMinusExpression : public UnaryExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct UnaryNotExpression : public UnaryExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct MultiplicativeExpression : public Binary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct MultiplyExpression : public MultiplicativeExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct DivideExpression : public MultiplicativeExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct ModuloExpression : public MultiplicativeExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct AdditiveExpression : public Binary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct AddExpression : public AdditiveExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct SubtractExpression : public AdditiveExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct RelationalExpression : public Binary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct LessExpression : public RelationalExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct LessEqualExpression : public RelationalExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct GreaterExpression : public RelationalExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct GreaterEqualExpression : public RelationalExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct EqualityExpression : public Binary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct EqualExpression : public EqualityExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct NotEqualExpression : public EqualityExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct LogicalAndExpression : public Binary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct LogicalOrExpression : public Binary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct AssignmentExpression : public Binary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct GlobalAssignmentExpression : public AssignmentExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct LocalAssignmentExpression : public AssignmentExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct AddAssignmentExpression : public AssignmentExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct SubAssignmentExpression : public AssignmentExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct MulAssignmentExpression : public AssignmentExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct DivAssignmentExpression : public AssignmentExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct ModAssignmentExpression : public AssignmentExpression<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct InitializerAssignmentExpression : public Variadic<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct Block : public Unary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct TableInitializer : public Variadic<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct ArrayInitializer : public Variadic<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct IdentifierList : public Variadic<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct Function : public Binary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct Tree : public Unary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct Root : public Variadic<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct TypeOf : public Unary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct SizeOf : public Unary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct DoImport : public Unary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };


        template<class Iter, class Value, class FuncType>
        struct StatementList : public Variadic<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct IfStatement : public Variadic<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct WhileStatement : public Binary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct ForStatement : public Variadic<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct ForEachStatement : public Variadic<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct ReturnStatement : public Variadic<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct ContinueStatement : public Nullary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct BreakStatement : public Nullary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct ExpressionStatement : public Unary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        template<class Iter, class Value, class FuncType>
        struct BuiltinFunction : public Nullary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
            std::function<void(Interpreter&, std::vector<Value>&)> function;
        };

        template<class Iter, class Value, class FuncType>
        struct Parens : public Unary<Iter,Value,FuncType>
        {
            virtual Value accept(FuncType &f)
            {
                return f(*this);
            }
        };

        class Parser;
    }

    template<typename T> class Table;

    template<typename T> class Array;

    template<typename Iter, typename FuncType>
    struct BaseValue
    {
            using NumberType        = double;
            using StringContainer   = std::string;
            using StringIterator    = StringContainer::iterator;
            using NumberContainer   = NumberType;
            using PointerContainer  = std::shared_ptr<Internal::ASTNode<Iter, BaseValue, FuncType>>;
            using FunctionContainer = std::shared_ptr<Internal::Function<Iter,BaseValue,FuncType>>;
            using TreeContainer     = PointerContainer;
            using ArrayContainer    = std::shared_ptr<Array<BaseValue>>;
            using TableContainer    = std::shared_ptr<Table<BaseValue>>;

            struct ProtoStringContainer
            {
                StringIterator begin;
                StringIterator end;
                size_t hash;

                ProtoStringContainer(StringIterator begin_, StringIterator end_):
                    begin(begin_),
                    end(end_),
                    hash(std::hash<StringContainer>()(StringContainer(begin_, end_)))
                {
                }
            };

            struct UserPtrContainer
            {
                void* realptr;

                UserPtrContainer(void* ptr): realptr(ptr)
                {
                }
            };

            class TagReturn
            {
            };

            class TagBreak
            {
            };

            class TagContinue
            {
            };

            enum class Type
            {
                Nil,    Number,   String,   ProtoString,
                Tree,   Function, Table,    Array,
                Return, Break,    Continue, UserPtr,
            };

        private:
            Type m_type;
            union
            {
                NumberContainer m_number;
                PointerContainer m_pointer;
                UserPtrContainer m_userptr;
                TableContainer m_table;
                ArrayContainer m_array;
                StringContainer m_strval;
                ProtoStringContainer m_protostr;
            };

        public:
            Type type() const
            {
                if(m_type == Type::ProtoString)
                {
                    return Type::String;
                }
                else if((m_type >= Type::Return) && (m_type <= Type::Continue))
                {
                    return Type::Nil;
                }
                return m_type;
            }

            const char* typeName() const
            {
                switch(type())
                {
                    case Type::Nil:
                        return "nil";
                    case Type::Number:
                        return "number";
                    case Type::String:
                        return "string";
                    case Type::ProtoString:
                        return "protostring";
                    case Type::Tree:
                        return "tree";
                    case Type::Function:
                        return "function";
                    case Type::Table:
                        return "table";
                    case Type::Array:
                        return "array";
                    case Type::Return:
                        return "return";
                    case Type::Break:
                        return "break";
                    case Type::Continue:
                        return "continue";
                    case Type::UserPtr:
                        return "userptr";
                    default:
                        ;
                }
                return "invalid";
            }

            Type internal_type() const
            {
                return m_type;
            }

            const NumberType& number() const
            {
                return m_number;
            }

            NumberType& number()
            {
                return m_number;
            }

            const PointerContainer& pointer() const
            {
                return m_pointer;
            }

            PointerContainer& pointer()
            {
                return m_pointer;
            }

            void*& userptr()
            {
                return m_userptr.realptr;
            }

            const void* userptr() const
            {
                return m_userptr.realptr;
            }

            const FunctionContainer function() const
            {
                return std::static_pointer_cast<
                    Internal::Function<Iter,BaseValue,FuncType>
                >(m_pointer);
            }

            FunctionContainer function()
            {
                return std::static_pointer_cast<
                    Internal::Function<Iter,BaseValue,FuncType>
                >(m_pointer);
            }

            const TreeContainer tree() const
            {
                return m_pointer;
            }

            TreeContainer tree()
            {
                return m_pointer;
            }

            const TableContainer& table() const
            {
                return m_table;
            }

            TableContainer& table()
            {
                return m_table;
            }

            const StringContainer string() const
            {
                if(m_type == Type::ProtoString)
                {
                    return std::string(m_protostr.begin, m_protostr.end);
                }
                return m_strval;
            }

            StringContainer& string()
            {
                decay();
                return m_strval;
            }

            const ArrayContainer& array() const
            {
                return m_array;
            }

            ArrayContainer& array()
            {
                return m_array;
            }

            void decay()
            {
                if(m_type == Type::ProtoString)
                {
                    StringIterator tmpbegin = m_protostr.begin;
                    StringIterator tmpend = m_protostr.end;
                    m_type = Type::String;
                    new (&m_strval) StringContainer(tmpbegin, tmpend);
                }
                else if((m_type >= Type::Return) && (m_type <= Type::Continue))
                {
                    m_type = Type::Nil;
                }
            }

            explicit BaseValue():
                m_type(Type::Nil), m_number(0)
            {
            }

            explicit BaseValue(const TagReturn&):
                m_type(Type::Return), m_number(0)
            {
            }

            explicit BaseValue(const TagBreak&):
                m_type(Type::Break), m_number(0)
            {
            }

            explicit BaseValue(const TagContinue&):
                m_type(Type::Continue), m_number(0)
            {
            }

            explicit BaseValue(NumberType value):
                m_type(Type::Number), m_number(value)
            {
            }

            explicit BaseValue(Type t, const PointerContainer &ptr):
                m_type(t), m_pointer(ptr)
            {
            }

            explicit BaseValue(UserPtrContainer ptr):
                m_type(Type::UserPtr), m_userptr(ptr.realptr)
            {
            }

            BaseValue(const TableContainer &ptr):
                m_type(Type::Table), m_table(ptr)
            {
            }

            BaseValue(const ArrayContainer &ptr):
                m_type(Type::Array), m_array(ptr)
            {
            }

            BaseValue(const StringContainer &value):
                m_type(Type::String), m_strval(value)
            {
            }

            BaseValue(StringIterator begin, StringIterator end):
                m_type(Type::ProtoString), m_protostr(begin, end)
            {
            }

            BaseValue(const BaseValue &that) : m_type(that.m_type)
            {
                switch(m_type)
                {
                    case Type::Return:
                    case Type::Break:
                    case Type::Continue:
                    case Type::Nil:
                        new (&m_number) NumberContainer(0);
                        break;
                    case Type::Number:
                        new (&m_number) NumberContainer(that.m_number);
                        break;
                    case Type::String:
                        new (&m_strval) StringContainer(that.m_strval);
                        break;
                    case Type::ProtoString:
                        new (&m_protostr) ProtoStringContainer(that.m_protostr);
                        break;
                    case Type::Tree:
                    case Type::Function:
                        new (&m_pointer) PointerContainer(that.m_pointer);
                        break;
                    case Type::Table:
                        new (&m_table) TableContainer(that.m_table);
                        break;
                    case Type::Array:
                        new (&m_array) ArrayContainer(that.m_array);
                        break;
                    case Type::UserPtr:
                        #if 0
                        fprintf(stderr, "BaseValue(BaseValue&): this.userptr=%p, that.userptr=%p\n",
                            m_userptr.realptr, that.m_userptr.realptr
                        );
                        #endif
                        new (&m_userptr) UserPtrContainer(that.m_userptr);
                        break;
                    default:
                        break;
                }
            }

            BaseValue& operator=(const BaseValue &that)
            {
                switch(m_type)
                {
                    case Type::String:
                        m_strval.~StringContainer();
                        break;
                    case Type::Tree:
                    case Type::Function:
                        m_pointer.~PointerContainer();
                        break;
                    case Type::Table:
                        m_table.~TableContainer();
                        break;
                    case Type::Array:
                        m_array.~ArrayContainer();
                        break;
                    default:
                        break;
                }
                m_type = that.m_type;
                switch(m_type)
                {
                    case Type::Return:
                    case Type::Break:
                    case Type::Continue:
                    case Type::Nil:
                        new (&m_number) NumberContainer(0);
                        break;
                    case Type::Number:
                        new (&m_number) NumberContainer(that.m_number);
                        break;
                    case Type::String:
                        new (&m_strval) StringContainer(that.m_strval);
                        break;
                    case Type::ProtoString:
                        new (&m_protostr) ProtoStringContainer(that.m_protostr);
                        break;
                    case Type::Tree:
                    case Type::Function:
                        new (&m_pointer) PointerContainer(that.m_pointer);
                        break;
                    case Type::UserPtr:
                        #if 0
                        fprintf(stderr, "BaseValue::operator=(BaseValue&): this.userptr=%p, that.userptr=%p\n",
                            m_userptr.realptr, that.m_userptr.realptr
                        );
                        #endif
                        new(&m_userptr) UserPtrContainer(that.m_userptr);
                        break;
                    case Type::Table:
                        new (&m_table) TableContainer(that.m_table);
                        break;
                    case Type::Array:
                        new (&m_array) ArrayContainer(that.m_array);
                        break;
                    default:
                        break;
                }
                return *this;
            }

            ~BaseValue()
            {
                switch(m_type)
                {
                    case Type::String:
                        m_strval.~StringContainer();
                        break;
                    case Type::Tree:
                    case Type::Function:
                        m_pointer.~PointerContainer();
                        break;
                    case Type::Table:
                        m_table.~TableContainer();
                        break;
                    case Type::Array:
                        m_array.~ArrayContainer();
                        break;
                    default:
                        break;
                }
            }

            bool operator==(const BaseValue &that) const
            {
                if(type() != that.type())
                {
                    return false;
                }
                switch(m_type)
                {
                    case Type::Return:
                    case Type::Break:
                    case Type::Continue:
                    case Type::Nil:
                    case Type::Number:
                        return number() == that.number();
                    case Type::Tree:
                    case Type::Function:
                        return pointer() == that.pointer();
                    case Type::Table:
                        return table() == that.table();
                    case Type::Array:
                        return array() == that.array();
                    case Type::UserPtr:
                        return (userptr() == that.userptr());
                    case Type::String:
                        if(that.m_type == Type::String)
                        {
                            return m_strval == that.m_strval;
                        }
                        else
                        {
                            if(m_strval.end()-m_strval.begin() != that.m_protostr.end - that.m_protostr.begin)
                            {
                                return false;
                            }
                            return std::equal(m_strval.begin(), m_strval.end(), that.m_protostr.begin);
                        }
                    case Type::ProtoString:
                        if(that.m_type == Type::String)
                        {
                            if(m_protostr.end - m_protostr.begin != that.m_strval.end() - that.m_strval.begin())
                            {
                                return false;
                            }
                            return std::equal(m_protostr.begin, m_protostr.end, that.m_strval.begin());
                        }
                        else
                        {
                            if(m_protostr.end - m_protostr.begin != that.m_protostr.end - that.m_protostr.begin)
                            {
                                return false;
                            }
                            return std::equal(m_protostr.begin, m_protostr.end, that.m_protostr.begin);
                        }
                    default:
                        return false;
                }
            }

            friend struct std::hash<BaseValue<Iter, FuncType>>;
    };
}

namespace std
{
    template<typename Iter, typename FuncType>
    struct hash<Aqua::BaseValue<Iter, FuncType>>
    {
        public:
            using T = Aqua::BaseValue<Iter, FuncType>;
            std::size_t operator()(Aqua::BaseValue<Iter, FuncType> const& x) const
            {
                switch(x.m_type)
                {
                    case T::Type::Nil:
                    case T::Type::Number:
                        return std::hash<typename T::NumberContainer>()(x.number());
                    case T::Type::String:
                        return std::hash<typename T::StringContainer>()(x.string());
                    case T::Type::ProtoString:
                        return x.m_protostr.hash;
                    case T::Type::UserPtr:
                        return std::hash<const void*>()(x.userptr());;
                    case T::Type::Tree:
                    case T::Type::Function:
                        return std::hash<typename T::PointerContainer>()(x.pointer());
                    case T::Type::Table:
                        return std::hash<typename T::TableContainer>()(x.table());
                    case T::Type::Array:
                        return std::hash<typename T::ArrayContainer>()(x.array());
                    default:
                        return 0;
                }
            }
    };
}

template<class Iter, class FuncType>
std::ostream& operator<<(std::ostream &out, const Aqua::BaseValue<Iter,FuncType> &v)
{
    using T = Aqua::BaseValue<Iter,FuncType>;
    switch(v.type())
    {
        case T::Type::Nil:
            return out << "nil";
        case T::Type::Number:
            return out << v.number();
        case T::Type::String:
            return out << v.string();
        case T::Type::Tree:
            return out << "tree@" << v.tree();
        case T::Type::Function:
            return out << "function@" << v.function();
        case T::Type::Table:
            return out << "table@" << v.table();
        case T::Type::Array:
            return out << "array@" << v.array();
        case T::Type::UserPtr:
            return out << "userpointer@" << v.userptr();
        default:
            return out;
    }
}

namespace Aqua
{
    template<typename Type> class Table
    {
        public:
            using Iterator = typename std::unordered_map<Type, Type>::iterator;

        private:
            std::unordered_map<Type, Type> m_data;
            Type m_nil;

        public:
            void set(const Type& key, const Type& value)
            {
                if(key.type() == Type::Type::Nil)
                {
                    return;
                }
                if(value.type() == Type::Type::Nil)
                {
                    m_data.erase(key);
                }
                m_data[key] = value;
            }

            Type get(const Type& key)
            {
                auto iter = m_data.find(key);
                if(iter == m_data.end())
                {
                    return m_nil;
                }
                else
                {
                    return iter->second;
                }
            }

            void append(const Table& that)
            {
                m_data.insert(that.m_data.begin(), that.m_data.end());
            }

            size_t size() const
            {
                return m_data.size();
            }

            Iterator begin()
            {
                return m_data.begin();
            }

            Iterator end()
            {
                return m_data.end();
            }
    };

    template<typename Type>
    class Array
    {
        public:
            using Iterator = typename std::vector<Type>::iterator;

        private:
            std::vector<Type> m_data;
            Type m_nil;

        public:
            void set(const Type& key, const Type& value)
            {
                int index = key.number();
                m_data[index] = value;
            }

            Type get(const Type& key)
            {
                int index = key.number();
                return m_data[index];
            }

            void append(const Array& that)
            {
                m_data.insert(m_data.end(), that.m_data.begin(), that.m_data.end());
            }

            void add(const Type& value)
            {
                m_data.push_back(value);
            }

            size_t size() const
            {
                return m_data.size();
            }

            Iterator begin()
            {
                return m_data.begin();
            }

            Iterator end()
            {
                return m_data.end();
            }
    };

    class Interpreter
    {
        public:
            using Iter           = std::vector<Internal::Token<std::string::iterator>>::iterator;
            using FuncType       = Interpreter;
            using Value          = BaseValue<Iter, FuncType>;
            using Environment    = std::shared_ptr<Table<Value>>;
            using EnvStack       = std::vector<Environment>;
            using FunStack       = std::vector<size_t>;
            using ParserInstance = Internal::Parser;
            using ModulePath     = std::vector<std::string>;
            using FuncProto      = std::function<void(Interpreter&, std::vector<Value>&)>;

            struct FunctionScope
            {
                Interpreter* m_self;

                FunctionScope(Interpreter* self): m_self(self)
                {
                    m_self->beginFunctionScope();
                }

                ~FunctionScope()
                {
                    m_self->endFunctionScope();
                }
            };

            struct LocalScope
            {
                Interpreter& m_self;

                LocalScope(Interpreter& self): m_self(self)
                {
                    m_self.beginLocalScope();
                }

                ~LocalScope()
                {
                    m_self.endLocalScope();
                }
            };

        private:
            ParserInstance* m_parser;
            std::vector<Value> m_function_stack;
            std::vector<std::unordered_map<std::string, Value>> m_scopes;
            EnvStack m_envstack;
            FunStack m_funstack;
            ModulePath m_modpath;

        public:
            Value operator()(Internal::Variable<Iter,Value,FuncType> &node);

            Value operator()(Internal::FieldName<Iter,Value,FuncType> &node);

            Value operator()(Internal::Constant<Iter,Value,FuncType> &node);

            Value operator()(Internal::Nop<Iter,Value,FuncType> &);

            Value operator()(Internal::Nil<Iter,Value,FuncType> &);

            Value operator()(Internal::String<Iter,Value,FuncType> &node);

            Value operator()(Internal::Parens<Iter,Value,FuncType> &node);

            Value operator()(Internal::ExpressionStatement<Iter,Value,FuncType> &node);

            Value operator()(Internal::TypeOf<Iter,Value,FuncType> &node);

            Value operator()(Internal::SizeOf<Iter,Value,FuncType> &node);

            Value operator()(Internal::DoImport<Iter,Value,FuncType>& node);

            Value operator()(Internal::FieldAccess<Iter,Value,FuncType> &node);

            Value operator()(Internal::FieldAssignment<Iter,Value,FuncType> &node);

            Value operator()(Internal::AddFieldAssignment<Iter,Value,FuncType> &node);

            Value operator()(Internal::SubFieldAssignment<Iter,Value,FuncType> &node);

            Value operator()(Internal::MulFieldAssignment<Iter,Value,FuncType> &node);

            Value operator()(Internal::DivFieldAssignment<Iter,Value,FuncType> &node);

            Value operator()(Internal::ModFieldAssignment<Iter,Value,FuncType> &node);

            Value operator()(Internal::UnaryPlusExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::UnaryMinusExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::UnaryNotExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::MultiplyExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::DivideExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::ModuloExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::AddExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::SubtractExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::LessExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::LessEqualExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::GreaterExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::GreaterEqualExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::EqualExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::NotEqualExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::LogicalAndExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::LogicalOrExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::AssignmentExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::GlobalAssignmentExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::LocalAssignmentExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::AddAssignmentExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::SubAssignmentExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::MulAssignmentExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::DivAssignmentExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::ModAssignmentExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::Block<Iter,Value,FuncType> &node);

            Value operator()(Internal::TableInitializer<Iter,Value,FuncType> &node);

            Value operator()(Internal::ArrayInitializer<Iter,Value,FuncType> &node);

            Value operator()(Internal::Function<Iter,Value,FuncType> &node);

            Value operator()(Internal::ReturnStatement<Iter,Value,FuncType> &node);

            Value operator()(Internal::IdentifierList<Iter,Value,FuncType> &node);

            Value operator()(Internal::BuiltinFunction<Iter,Value,FuncType> &node);

            Value operator()(Internal::Call<Iter,Value,FuncType> &node);

            Value operator()(Internal::MemberCall<Iter,Value,FuncType> &node);

            //Value operator()(Internal::EvalStatement<Iter,Value,FuncType> &node);

            Value operator()(Internal::Eval<Iter,Value,FuncType> &node);

            Value operator()(Internal::Tree<Iter,Value,FuncType> &node);

            Value operator()(Internal::Root<Iter,Value,FuncType> &node);

            Value operator()(Internal::ParseStatement<Iter,Value,FuncType> &node);

            Value operator()(Internal::ParseExpression<Iter,Value,FuncType> &node);

            Value operator()(Internal::StatementList<Iter,Value,FuncType> &node);

            Value operator()(Internal::IfStatement<Iter,Value,FuncType> &node);

            Value operator()(Internal::WhileStatement<Iter,Value,FuncType> &node);

            Value operator()(Internal::ForStatement<Iter,Value,FuncType> &node);

            Value operator()(Internal::ForEachStatement<Iter,Value,FuncType> &node);

            Value operator()(Internal::ContinueStatement<Iter,Value,FuncType> &);

            Value operator()(Internal::BreakStatement<Iter,Value,FuncType> &);


            template<class Type>
            Value operator()(Type& node)
            {
                error("unimplemented ast node.", node);
                //static_assert(false, "unimplemented AST node!");
                return Value();
            }


            template<typename Type>
            void error(const std::string text, Type& node)
            {
                std::stringstream strm;
                auto lc = Utils::LineColumn(node.source->begin(), node.begin->begin);
                auto from = node.begin;
                auto to = node.end-1;
                auto line = lc.first;
                auto column = lc.second;
                auto source = std::string(from->begin, to->end);
                strm << "<input>:" << line << ":" << column << " error: "  << text << " in: " << source;
                throw std::runtime_error(strm.str());
            }

            template<typename Type>
            Value makeUserPointer(Type* ptr)
            {
                return Value(Value::UserPtrContainer((void*)ptr));
            }

            Value makeString(const std::string& src);

            Value makeNumber(Value::NumberType val);

            std::shared_ptr<Array<Value>> makeArray();

            std::shared_ptr<Table<Value>> makeTable();

            Value makeFunction(FuncProto fun);

            bool need(const Value& raw_in, Value& dest, Value::Type expected);

            bool needNumber(const Value& raw_in, Value& dest);

            bool needString(const Value& raw_in, Value& dest);

            bool needFunction(const Value& raw_in, Value& dest);

            void beginLocalScope();

            void endLocalScope();

            void beginFunctionScope();

            void endFunctionScope();

            Value get(Value key);

            void set(Value key, Value value);

            void setLocal(Value key, Value value);

            void setGlobal(Value key, Value value);

            void setGlobalFunc(const std::string &name, FuncProto fun);

            ModulePath getModulePath();

            void addModulePath(const std::string& path);

            Interpreter();

            ~Interpreter();

            Value eval(const std::string &source);

            Value run(const std::string &source);
    };
}
